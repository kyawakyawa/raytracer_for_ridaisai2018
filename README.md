# raytracer for ridaisai2018
![book](pictures/result000.png)

## Requirement

- OS
    - Linux
- Compiler
    - clang


## Build

```bash=
$ make -j
```

## Run

```bash=
$ ./exexec.out
```
