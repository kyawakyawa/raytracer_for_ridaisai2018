#pragma once

#include "Core.hpp" 

#include<string>
#include<vector>

namespace ridaisai2018 {
class ImageIO {
  public: 

  ImageIO();

  ImageIO(const std::vector<Color> &img,const int width,const int height,const int channel = 3);

  bool set_image(const std::vector<Color> &img,const int width,const int height,const int channel = 3);

  bool load(const std::string &filename);

  bool write_png(const std::string &filename,const bool gamma_correction_on = true) const;

  bool write_hdr(const std::string &filename) const;

  ~ImageIO();

  private:

  inline void linerTosRGB(std::vector<Color> &img) const;
    
  std::vector<Color> img_;
  int width_;
  int height_;

  int channel_;
};
}
