#pragma once

#include "Core.hpp"
#include "Mesh.hpp"

namespace ridaisai2018 {

class Triangle_bbox : public nanort::TriangleMesh<R> {
public:
  Triangle_bbox(const Mesh &mesh) : nanort::TriangleMesh<R>(mesh.get_vertices(),mesh.get_vertice_index(),sizeof(R) * 3) {}
};

}
