#pragma once

#include <vector>
#include <numeric>

#ifdef _DEBUG
#include <iostream>
#endif

#include "Core.hpp"
#include "Camera.hpp"
#include "Xor128.hpp"

namespace ridaisai2018{

class Thin_lens_camera : Camera {
public:
  Thin_lens_camera();
  Thin_lens_camera(const int threads
                  ,const Vec3 &position_,const Vec3 &dir_,const Vec3 &up_,const R fov
                  ,const int pixel_w,const int pixel_h
                  ,const R focus_distane,const R f_number
                  );

  ~Thin_lens_camera();

  void init(const int threads
           ,const Vec3 &position_,const Vec3 &dir_,const Vec3 &up_,const R fov
           ,const int pixel_w,const int pixel_h
           ,const R focus_distane,const R f_number
           );

  inline void sample_z0_and_next_direction(const int i,const int j,const int thread_id
                                ,Vec3 &z0,Vec3 &normal_z0,R &pdf_A_z0,Color &alpha_E_1//始点(レンズ上)の情報
                                ,Vec3 &dir_to_z1,R &pdf_sigma_to_z1,Color &alpha_E_2//次の点に向かう方向や確率密度(立体角速度)
                                ,R &cos_//z0の法線とdir_to_z1が成す角のcos
                                ) const {

#ifdef _DEBUG
    if(i < 0 || i >= pixel_h_ || j < 0 || j >= pixel_w_) {
      std::cerr << "warning sample and get ray :" << i << " " << j << std::endl;
      return;
    }
#endif

    sample_one_point_on_lens(thread_id,z0,pdf_A_z0);
    normal_z0 = dir_;

    alpha_E_1 = Color(sensibility_) / pdf_A_z0;

    Vec3 zp;
    R pdf_A_zp;
    sample_one_point_on_image_sensor(i,j,thread_id,zp,pdf_A_zp);
    const Vec3 e = nanort::vnormalize(position_ - zp);

    const Vec3 z1 = position_ + e * ( (dist_lens_object_plane_) / nanort::vdot(e,dir_) );//オブジェクトプレーン上の仮想のz1

    dir_to_z1 = nanort::vnormalize(z1 - z0);
    cos_ = vdot(dir_to_z1,normal_z0);

    R dp_square = dist_sensor_lens_ * dist_sensor_lens_;

    pdf_sigma_to_z1 = dp_square * pdf_A_zp / Math::pow<4>(cos_);

    alpha_E_2 = alpha_E_1 * Math::pow<4>(vdot(vnormalize(z0 - zp),normal_z0)) * cos_ / dp_square / pdf_A_zp;

  }

  inline bool get_intersection_with_lens(const nanort::Ray<R> &ray,Intersection<R> &isect,R &cos_sensor,R &cos_object_plane,int &i,int&j) const {

    const bool hit = intersection_plane(ray,position_,isect);

    const Vec3 &&x0 = Vec3(ray.org) + isect.t * Vec3(ray.dir);

    if(!hit || isect.t < ray.min_t - Constant::kEPS || ray.max_t + Constant::kEPS < isect.t || vlength(x0 - position_) > lens_radius_) {
      return false;
    }

    R object_plane_t = static_cast<R>(0.0);
    if (!fetch_object_plane_ij(ray,i,j,object_plane_t)) {
      return false;
    }

    const Vec3 &&object_plane_position = Vec3(ray.org) + object_plane_t * Vec3(ray.dir);

    const Vec3 &&omega_object_plane_lens_center = nanort::vnormalize(position_ - object_plane_position);

    const R cos_ = -nanort::vdot(omega_object_plane_lens_center,dir_);

    const Vec3 &&xp = position_ + omega_object_plane_lens_center * (dist_sensor_lens_ / cos_);

    cos_sensor = nanort::vdot(nanort::vnormalize(x0 - xp),dir_);
    cos_object_plane = nanort::vdot(nanort::vnormalize(-Vec3(ray.dir)),dir_);

    return true;
  }

  inline int get_pixel_w() const {
    return pixel_w_;
  }

  inline int get_pixel_h() const {
    return pixel_h_;
  }

  inline int get_threads() const {
    return threads_;
  }

  inline Vec3 get_dir() const {
    return dir_;
  }

  inline int get_dist_sensor_lens() const {
    return dist_sensor_lens_;
  }

  inline R get_pdf_i() const {
    return pdf_i_;
  }

  inline void add_contribution_img_l(const Color rgb,const int i,const int j,const int thread_id) {

#ifdef _DEBUG
    if (i < 0 || i >= pixel_h_ || j < 0 || j >= pixel_w_ || thread_id < 0 || thread_id >= threads_) {
      std::cerr << "warning in func add_contribution_img_l :" << i << " " << j << " " << thread_id << std::endl; 
      return;
    }
#endif

    img_l_[thread_id][i * pixel_w_ + j] += rgb;

  }

  inline void add_contribution_img_e(const Color rgb,const int i,const int j,const int thread_id) {

#ifdef _DEBUG
    if (i < 0 || i >= pixel_h_ || j < 0 || j >= pixel_w_ || thread_id < 0 || thread_id >= threads_) {
      std::cerr << "warning in func add_contribution_img_e :" << i << " " << j << " " << thread_id << std::endl; 
      return;
    }
#endif

    img_e_[thread_id][i * pixel_w_ + j] += rgb;

  }

  inline void add_count_l(const int thread_id) {
#ifdef _DEBUG
    if (thread_id < 0 || thread_id >= threads_) {
      std::cerr << "warning in func add_count_e :" << thread_id << std::endl; 
      return;
    }
#endif
    sample_count_l_[thread_id]++;
  }

  inline void add_count_e(const int i,const int j,const int thread_id) {
#ifdef _DEBUG
    if (i < 0 || i >= pixel_h_ || j < 0 || j >= pixel_w_ || thread_id < 0 || thread_id >= threads_) {
      std::cerr << "warning in func add_contribution_img_e :" << i << " " << j << " " << thread_id << std::endl; 
      return;
    }
#endif
    sample_count_e_[thread_id][i* pixel_w_ + j]++;
  }

  void out_rgb(std::vector<Color> &rgb) const {
    std::vector<Color> rgb_l(pixel_w_ * pixel_h_,static_cast<R>(0));

    const int sample_l_sum = std::accumulate(sample_count_l_.begin(),sample_count_l_.end(),0);

    if(sample_l_sum > 0) {

      for (int i = 0;i < pixel_h_;i++) {
        for (int j = 0;j < pixel_w_;j++) {
          int pixel_id = i * pixel_w_ + j;
          for (int thread_id = 0;thread_id < threads_;thread_id++) {
            rgb_l[pixel_id] += img_l_[thread_id][pixel_id] / static_cast<R>(sample_l_sum);
          }
        }
      }
    }

    std::vector<Color> rgb_e(pixel_w_ * pixel_h_,static_cast<R>(0));

    for (int i = 0;i < pixel_h_;i++) {
      for (int j = 0;j < pixel_w_;j++) {
        int sample_e_sum = 0;
        int pixel_id = i * pixel_w_ + j;

        for (const auto &v : sample_count_e_) {
          sample_e_sum += v[pixel_id];
        }

        if (sample_e_sum > 0) {

          for (int thread_id = 0;thread_id < threads_;thread_id++) {
            rgb_e[pixel_id] += img_e_[thread_id][pixel_id] / static_cast<R>(sample_e_sum);
          }

        }
      }
    }

    rgb.clear();

    rgb.resize(pixel_w_ * pixel_h_);

    for (int i = 0;i < pixel_h_;i++) {
      for (int j = 0;j < pixel_w_;j++) {
        rgb[(pixel_h_ - i - 1) * pixel_w_ + (pixel_w_ - j - 1)] = rgb_l[i * pixel_w_ + j] + rgb_e[i * pixel_w_ + j];
      }
    }

#ifdef _DEBUG
    for (auto v : rgb) {
      if (std::isnan(v[0]) || std::isnan(v[1]) || std::isnan(v[2]) || std::isinf(v[0]) || std::isinf(v[1]) || std::isinf(v[2])) {
        std::cerr << "warning rgb inf" << std::endl;
      }
    }
#endif

  }

private:

  inline bool fetch_object_plane_ij(const nanort::Ray<R> &ray,int &i,int &j,R &object_plane_t) const {
    Intersection<R> isect;
    const bool hit = intersection_plane(ray,object_center_,isect);

    if (!hit) {
      return false;
    }

    object_plane_t = isect.t;

    //if (obj_t < ray.min_t - Constant::kEPS || ray.max_t + Constant::kEPS < obj_t) {
    //  return false;
    //}

    const Vec3 &&position_on_obj = Vec3(ray.org) + Vec3(ray.dir) * object_plane_t;

    const Vec3 &&position_on_uv = position_on_obj - object_center_;//uv座標上の位置ベクトル

    const R u = nanort::vdot(position_on_uv,object_plane_x_) / width_of_object_plane_;//u成分を得る
    const R v = nanort::vdot(position_on_uv,object_plane_y_) / height_of_object_plane_;//v成分を得る

    if(u < -static_cast<R>(0.5) || u > static_cast<R>(0.5) || v < -static_cast<R>(0.5) || v > static_cast<R>(0.5)) {//オブジェクトプレーンの外
      return false;
    }

    j = (static_cast<R>(0.5) + u) * static_cast<R>(pixel_w_);
    i = (static_cast<R>(0.5) + v) * static_cast<R>(pixel_h_);

#ifdef _DEBUG
    if(j < 0 || pixel_w_ <= j || i < 0 || pixel_h_ <= i) {
      std::cerr << "warning in func fetch_object_plane_ij" << std::endl;
    }
#endif

    return true;
  }

  inline void sample_one_point_on_lens(const int thread_id,Vec3 &p,R &pdf) const {
#ifdef _DEBUG
    if(thread_id < 0 || thread_id >= threads_)
    std::cerr << "warning sample one point on lens   thread_id :" << thread_id << std::endl;
#endif

    R u1,u2;

    do {
      u1 = xor128s_[thread_id].rnd();
      u2 = xor128s_[thread_id].rnd();
    } while (u1 * u1 + u2 * u2 > static_cast<R>(1.0));

    p = position_ + u1 * lens_radius_ * x_ + u2 * lens_radius_ * y_;

    pdf = pdf_l_;

  }

  inline void sample_one_point_on_image_sensor(const int i,const int j,const int thread_id,Vec3 &p,R &pdf) const {

#ifdef _DEBUG
    if(i < 0 || i >= pixel_h_ || j < 0 || j >= pixel_w_) {
      std::cerr << "warning" << std::endl;
      p = Vec3(0,0,0);
      pdf = pdf_i_;
      return;
    }
#endif

    const R dx = xor128s_[thread_id].rnd();
    const R dy = xor128s_[thread_id].rnd();

    p = sensor_center_
      + x_ * width_of_sensor_ * ((static_cast<R>(j) + dx) / static_cast<R>(pixel_w_) - static_cast<R>(0.5))
      + y_ * height_of_sensor_ * ((static_cast<R>(i) + dy) / static_cast<R>(pixel_h_) - static_cast<R>(0.5));

    pdf = pdf_i_;

  }

  inline bool intersection_plane(const nanort::Ray<R> &ray,const Vec3 &position,Intersection<R> &isect) const {
    const Vec3 &&d = Vec3(ray.dir);
	  Vec3 s2;

	  if(std::abs(dir_[0]) > 1e-3)
		  s2 = Vec3(-(dir_[1] + dir_[2]) / dir_[0],static_cast<R>(1.0),static_cast<R>(1.0));
	  else if(std::abs(dir_[1]) > 1e-3)
		  s2 = Vec3(static_cast<R>(1.0),-(dir_[2] + dir_[0]) / dir_[1],static_cast<R>(1.0));
    else
		  s2 = Vec3(static_cast<R>(1.0),static_cast<R>(1.0),-(dir_[0] + dir_[1]) / dir_[2]);

	  const Vec3 &&s = Vec3(ray.org) - (s2 + position);

	  if(nanort::vdot(d,dir_) == static_cast<R>(0.0))//レイと平面が並行
		  return false;


    if(nanort::vdot(d,dir_) > static_cast<R>(0.0))//平面の裏側から当たる
      return false;

	  isect.t = -nanort::vdot(s,dir_) / nanort::vdot(d,dir_);

    return true;
  }

  int threads_; // CPUのthread数

  Vec3 position_; //レンズの中心の位置
  Vec3 dir_;
  Vec3 up_;

  int pixel_w_; //画素数(横)
  int pixel_h_; //画素数(縦)

  std::vector<int> sample_count_l_; // 光源側からレイを飛ばした回数 CPUのtherad数分用意

  std::vector<std::vector<int>> sample_count_e_; // 視線側からレイを飛ばした回数 [thread num][pixel num]

  std::vector<std::vector<Color>> img_l_;// t = 0,1 の時の結果の記憶 CPUのtherad数分用意 [thread num][pixel num]
  std::vector<std::vector<Color>> img_e_;// t >= 2 の時の結果の記憶 CPUのtherad数分用意 [thread num][pixel num]

  Vec3 x_,y_;
  Vec3 object_plane_x_,object_plane_y_;

  R width_of_sensor_;
  R height_of_sensor_;
  R dist_sensor_lens_;
  Vec3 sensor_center_;

  R focal_length_;//レンズの焦点距離
  R lens_radius_;//レンズの半径
  R f_number_;

  R width_of_object_plane_;
  R height_of_object_plane_;
  R dist_lens_object_plane_;
  Vec3 object_center_;

  R pdf_i_;
  R pdf_l_;

  R sensibility_;

  std::vector<Xor128> xor128s_;
};

}
