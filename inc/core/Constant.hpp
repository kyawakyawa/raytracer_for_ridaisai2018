#pragma once

#include "Typedef.hpp"
namespace ridaisai2018 {
  namespace Constant {

    static constexpr R kPI = 3.141592653589793238462643383279502884197;
    static constexpr R kEPS = 1e-9;

  }
}
