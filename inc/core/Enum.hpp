#pragma once

namespace ridaisai2018 {

enum SHAPE_TYPE {
  ST_SHAPE,
  ST_MESH
};

enum BXDF_TYPE {
  BT_BSDF,
};

enum LIGHT_TYPE {
  LT_AREA_LIGHT,
  LT_NO_LIGHT,
};

}
