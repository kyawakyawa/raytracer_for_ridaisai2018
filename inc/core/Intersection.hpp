#pragma once

#include "Enum.hpp"
#include "Typedef.hpp"

namespace ridaisai2018 {

template <typename T = R>                                                                                                                                                                                 
class Intersection {                                                                                                                                                                                  
public:                                                                                                                                                                                                      
  T u;
  T v;

  T t;

  unsigned int prim_id;

  SHAPE_TYPE shape_type;

  unsigned int shape_id;
  
};

}
