#pragma once

#include "Typedef.hpp"
#include "Enum.hpp"

namespace ridaisai2018 {

class Light_info {
public:

  R pdf_A;
  R pdf_sigma;

  Vec3 p;

  Vec3 normal;

  LIGHT_TYPE light_type;
  unsigned int light_id;

  SHAPE_TYPE shape_type;// only use if light_type equals LT_AREA_LIGHT
  unsigned int shape_id;// only use if light_type equals LT_AREA_LIGHT
  unsigned int prim_id;// only use if light_type equals LT_AREA_LIGHT

};

}
