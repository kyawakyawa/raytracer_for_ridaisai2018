#pragma once

#include "Typedef.hpp"
#include "Enum.hpp"

namespace ridaisai2018 {

class Material_info {
public:
  R u;
  R v;

  Vec3 p;

  Vec3 normal;

  Vec3 dpdu;
  Vec3 dpdv;

  bool is_entering = true;

  SHAPE_TYPE shape_type;
  unsigned int shape_id;
  unsigned int prim_id;

  unsigned int material_id;
};
}
