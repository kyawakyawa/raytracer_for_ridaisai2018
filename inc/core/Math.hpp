#pragma once

#include "Typedef.hpp"

namespace ridaisai2018 {

  namespace Math {

  template <int n>
  constexpr inline R pow(R v) {
  #ifdef _DEBUG
    static_assert(n > 0, "Power can't be negative");
  #endif
  
    R n2 = pow<n / 2>(v);
    return n2 * n2 * pow<n & 1>(v);
  }

  template<>
  constexpr inline R pow<1>(R v) {
    return v;
  }

  template<>
  constexpr inline R pow<0>(R v) {
    return 1;
  }

}

}
