#pragma once

#include "nanort.h"

namespace ridaisai2018 {
  using R = double;
  using Vec3 = nanort::real3<R>;
  using Color = nanort::real3<R>;
}
