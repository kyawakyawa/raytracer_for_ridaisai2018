#pragma once 

#include "Typedef.hpp"

namespace ridaisai2018 {

  namespace Utils {

    inline R luminance(const Color &c) {
      const R YWeight[3] = {static_cast<R>(0.212671), static_cast<R>(0.715160), static_cast<R>(0.072169)};;

      return YWeight[0] * c[0] + YWeight[1] * c[1] + YWeight[2] * c[2];
    }

    inline void generate_orthonormal_basis_from_normal(Vec3 &u,Vec3 &v,const Vec3 &normal) {
      if (std::abs(normal[0]) > static_cast<R>(1e-3)) 
    		u = vnormalize(vcross(Vec3(0.0, 1.0, 0.0), normal));
    	else
    		u = vnormalize(vcross(Vec3(1.0, 0.0, 0.0), normal));

    		v = vcross(normal,u);
    }

  }

}
