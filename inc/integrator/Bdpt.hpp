#pragma once

#include "Core.hpp"
#include "Scene.hpp"
#include "Intersector.hpp"
#include "Light_sampling.hpp"
#include "Integrator.hpp"
#include "Xor128.hpp"

#include "BXDF.hpp"
#include "BSDF.hpp"

#include <vector>
#include <omp.h>

namespace ridaisai2018 {

struct Vertex_data {
  LIGHT_TYPE light_type;
  Material_info material_info;
  Color alpha;
  R p_A_light;
  bool p_A_light_delta_flag;
  R russian_roulette_light;
  R p_A_eye;
  bool p_A_eye_delta_flag;
  R russian_roulette_eye;

  Vertex_data(){}

  Vertex_data (const LIGHT_TYPE light_type_
              ,const Material_info &material_info_
              ,const Color &alpha_
              ,const R p_A_light_,const bool p_A_light_delta_flag_,const R russian_roulette_light_
              ,const R p_A_eye_,const bool p_A_eye_delta_flag_,const R russian_roulette_eye_
              ) 

             : light_type(light_type_)
              ,material_info(material_info_)
              ,alpha(alpha_)
              ,p_A_light(p_A_light_),p_A_light_delta_flag(p_A_light_delta_flag_),russian_roulette_light(russian_roulette_light_)
              ,p_A_eye(p_A_eye_),p_A_eye_delta_flag(p_A_eye_delta_flag_),russian_roulette_eye(russian_roulette_eye_) {}
};

class Bdpt : public Intergrator {
public:
  Bdpt () = delete;
  Bdpt (Scene &scene);
  ~Bdpt();

  void init(Scene &scene);

  inline bool rendering(Scene &scene,int samples = 1) const {

    const int threads = scene.camera.get_threads();

#pragma omp parallel for schedule(dynamic, 1) num_threads(threads)
    for (int y = 0;y < scene.camera.get_pixel_h();y++) {
      for (int x = 0;x < scene.camera.get_pixel_w();x++) {
        for (int k = 0;k < samples;k++) {
          const int thread_id = omp_get_thread_num();
          light_tracing(scene,y,x,thread_id);
          path_tracing(scene,y,x,thread_id);
          merge(scene,y,x,thread_id);

          scene.camera.add_count_l(thread_id);
          scene.camera.add_count_e(y,x,thread_id);
        }
      }
    }

    return true;
  }

  inline void light_tracing(Scene &scene,const int y,const int x,const int thread_id) const {

    auto &lspv = light_sub_path_vertices_[thread_id];
    const auto &xor128 = xor128s_[thread_id];

    lspv.clear();
    lspv.reserve(min_num_vertices_of_sub_path_ * 4);

    Light_info light_info;
    Light_sampling::all_light_sampling(scene,xor128,light_info);

    const R pdf_A_y0 = light_info.pdf_A;

    Color alpha;
    if (light_info.light_type == LT_AREA_LIGHT) {
      alpha = scene.area_lights[light_info.light_id].get_emission();
    }

    alpha = alpha / pdf_A_y0;

    const Vec3 &normal = light_info.normal;

    Material_info material_info;

    material_info.u = static_cast<R>(0.0);//dummy
    material_info.v = static_cast<R>(0.0);//dummy
    material_info.p = light_info.p;
    material_info.normal = normal;

    material_info.shape_type = ST_SHAPE;
    material_info.shape_id = -1;
    material_info.prim_id = -1;

    if (light_info.light_type == LT_AREA_LIGHT) {
      material_info.shape_type = light_info.shape_type;
      material_info.shape_id = light_info.shape_id;
      material_info.prim_id = light_info.prim_id;
    }

    material_info.material_id = light_material_id_;

    lspv.emplace_back (
         light_info.light_type
        ,material_info
        ,alpha
        ,pdf_A_y0
        ,false
        ,static_cast<R>(1.0)
        ,static_cast<R>(-1e6)//Interim
        ,false//Interim
        ,static_cast<R>(-1e6)//Interim
        );

    Vec3 u,v;

    Utils::generate_orthonormal_basis_from_normal(u,v,normal);

    BSDF *bsdf = static_cast<BSDF*>(scene.materials[light_material_id_].get());

    Vec3 w_o;
    R pdf;
    bsdf->sample_f_s(material_info,w_o,Vec3(static_cast<R>(0.0),static_cast<R>(0.0),static_cast<R>(1.0)),xor128,pdf).x();

    Vec3 dir = u * w_o[0] + v * w_o[1] + normal * w_o[2];

    alpha = alpha * Constant::kPI;
    R previous_p_sigma = pdf;
    R previous_cos = w_o[2];

    nanort::Ray<R> ray;
    ray.org[0] = light_info.p[0];
    ray.org[1] = light_info.p[1];
    ray.org[2] = light_info.p[2];

    ray.dir[0] = dir[0];
    ray.dir[1] = dir[1];
    ray.dir[2] = dir[2];

    R kFar = static_cast<R>(1.0e+30);
    ray.min_t = static_cast<R>(0.0);
    ray.max_t = kFar;

    SHAPE_TYPE last_hit_shape_type = ST_SHAPE;
    unsigned int last_hit_shape_id = -1;
    unsigned int last_hit_prim_id = -1;

    if (light_info.light_type == LT_AREA_LIGHT) {
      last_hit_shape_type = light_info.shape_type;
      last_hit_shape_id = light_info.shape_id;
      last_hit_prim_id = light_info.prim_id;
    }

    get_sub_path(scene,alpha,previous_p_sigma,previous_cos,ray,lspv,y,x,thread_id,true,last_hit_shape_type,last_hit_shape_id,last_hit_prim_id);

  }

  inline void path_tracing(Scene &scene,const int y,const int x,const int thread_id) const {

    auto &espv = eye_sub_path_vertices_[thread_id];

    espv.clear();
    espv.reserve(min_num_vertices_of_sub_path_ * 4);

    Vec3 z0;Vec3 normal_z0;R pdf_A_z0;Color alpha_E_1;
    Vec3 dir_to_z1;R pdf_sigma_to_z1;Color alpha_E_2;
    R cos_;

    scene.camera.sample_z0_and_next_direction(y,x,thread_id
        ,z0,normal_z0,pdf_A_z0,alpha_E_1
        ,dir_to_z1,pdf_sigma_to_z1,alpha_E_2
        ,cos_);

    Material_info material_info;

    material_info.u = static_cast<R>(0.0);//dummy
    material_info.v = static_cast<R>(0.0);//dummy
    material_info.p = z0;
    material_info.normal = normal_z0;

    material_info.shape_type = ST_SHAPE;
    material_info.shape_id = -1;
    material_info.prim_id = -1;

    material_info.material_id = eye_material_id_;

    espv.emplace_back (
        LT_NO_LIGHT
        ,material_info
        ,alpha_E_1
        ,static_cast<R>(-1e6)//Interim
        ,false//Interim
        ,static_cast<R>(-1e6)//Interim
        ,pdf_A_z0
        ,false
        ,static_cast<R>(1.0)
        );

    const auto &alpha = alpha_E_2;

    R previous_p_sigma = pdf_sigma_to_z1;
    R previous_cos = cos_;

    nanort::Ray<R> ray;

    ray.org[0] = z0[0];
    ray.org[1] = z0[1];
    ray.org[2] = z0[2];

    ray.dir[0] = dir_to_z1[0];
    ray.dir[1] = dir_to_z1[1];
    ray.dir[2] = dir_to_z1[2];

    R kFar = static_cast<R>(1.0e+30);
    ray.min_t = static_cast<R>(0.0);
    ray.max_t = kFar;

    get_sub_path(scene,alpha,previous_p_sigma,previous_cos,ray,espv,y,x,thread_id,false);

  }

  inline void get_sub_path(Scene &scene
                   ,Color alpha,R previous_p_sigma,R previous_cos
                   ,nanort::Ray<R> ray
                   ,std::vector<Vertex_data> &spv
                   ,const int y,const int x,const int thread_id
                   ,const bool is_light_tracing
                   ,SHAPE_TYPE last_hit_shape_type = ST_SHAPE
                   ,unsigned int last_hit_shape_id = -1
                   ,unsigned int last_hit_prim_id = -1) const {

    auto &xor128 = xor128s_[thread_id];
    bool previous_flag = false;
    R russian_roulette_probability = static_cast<R>(1.0);

    while (true) {
      Intersection isect;

      bool hit = Intersector::all_intersector(ray,scene,isect,last_hit_shape_type,last_hit_shape_id,last_hit_prim_id);

#ifdef T_EQUAL_0
      if (is_light_tracing) {
          Intersection<R> isect;
          R cos_sensor,cos_object_plane;
          int y_dash,x_dash;

          if (scene.camera.get_intersection_with_lens(ray,isect,cos_sensor,cos_object_plane,y_dash,x_dash)) {
            std::vector<Vertex_data> pv(spv.size());

            for (int i = 0;i < spv.size();i++) {
              pv[i] = spv[i]; 
            }
            R dist2 = isect.t * isect.t;
            pv.back().p_A_eye = p_A_measure_from_camera(scene,cos_sensor,previous_cos,dist2);

            pv.back().p_A_eye_delta_flag = false;

            const Vec3 &&dir = vnormalize(-Vec3(ray.dir)); 

            R cos_o = nanort::vdot(dir , scene.camera.get_dir());
#ifdef _DEBUG
            if (cos_o < -Constant::kEPS) {
              std::cerr << "warning in get_subputh" << std::endl;
            }
#endif
            const R P = previous_p_sigma * std::abs(cos_o) / dist2;

            Material_info material_info;

            material_info.u = static_cast<R>(0.0);//dummy
            material_info.v = static_cast<R>(0.0);//dummy
            material_info.p = Vec3(ray.org) + isect.t * Vec3(ray.dir);
            material_info.normal = scene.camera.get_dir();

            material_info.shape_type = ST_SHAPE;
            material_info.shape_id = -1;
            material_info.prim_id = -1;

            material_info.material_id = eye_material_id_;

            pv.emplace_back(
                LT_NO_LIGHT
                ,material_info
                ,alpha
                ,P
                ,false
                ,static_cast<R>(1.0)
                ,static_cast<R>(0.0)
                ,false
                ,static_cast<R>(0.0)
              );
            //TODO 反対方向の確率密度更新

            scene.camera.add_contribution_img_l(alpha * (Math::pow<4>(cos_sensor / cos_object_plane) * get_weight(pv,pv.size(),0)),y_dash,x_dash,thread_id);
            break;
          }

      }
#endif

      if(!hit) {
        break;
      }

      Material_info material_info;

      update_material_info(material_info,scene,ray,isect);

      Light_info light_info;

      update_light_info(light_info,scene,ray,isect);

      update_last_hit(last_hit_shape_type,last_hit_shape_id,last_hit_prim_id,isect);

      const Vec3 &&dir = vnormalize(-Vec3(ray.dir)); 

      R cos_o = nanort::vdot(dir , material_info.normal);

      if (cos_o < static_cast<R>(0.0)) {
        material_info.normal = -material_info.normal;
        material_info.is_entering = false;
        cos_o = -cos_o;
      }

      const R dist2 = isect.t * isect.t;

      const R P = previous_p_sigma * std::abs(cos_o) / dist2;

      spv.emplace_back(
         LT_NO_LIGHT
        ,material_info
        ,alpha
        ,(is_light_tracing) ? P : static_cast<R>(-1e6)
        ,(is_light_tracing) ? previous_flag : false
        ,(is_light_tracing) ? russian_roulette_probability : static_cast<R>(-1e6)
        ,(!is_light_tracing) ? P : static_cast<R>(-1e6)
        ,(!is_light_tracing) ? previous_flag : false
        ,(!is_light_tracing) ? russian_roulette_probability : static_cast<R>(-1e6)
      );

      if (!is_light_tracing) {
        if (material_info.is_entering && light_info.light_type == LT_AREA_LIGHT) {
          const unsigned int light_id = light_info.light_id;

          std::vector<Vertex_data> pv;

          pv.resize(spv.size());

          for (int i = 0;i < spv.size();i++) {
            pv[i] = spv[static_cast<int>(spv.size()) - i - 1];
          }

          pv[0].p_A_light = (scene.light_F[LT_AREA_LIGHT][light_id + 1] - scene.light_F[LT_AREA_LIGHT][light_id]) * light_info.pdf_A;
          pv[0].p_A_light_delta_flag = false;
          pv[0].russian_roulette_light = static_cast<R>(1.0);

          Material_info material_info_light;
      
          material_info_light.u = static_cast<R>(0.0);//dummy
          material_info_light.v = static_cast<R>(0.0);//dummy
          material_info_light.p = material_info.p;
          material_info_light.normal = material_info.normal;
      
          material_info_light.shape_type = light_info.shape_type;
          material_info_light.shape_id = light_info.shape_id;
          material_info_light.prim_id = light_info.prim_id;
      
          material_info_light.material_id = light_material_id_;

          pv[1].p_A_light = p_A_measure(scene,material_info_light,Vec3(static_cast<R>(0.0)),dir,previous_cos,dist2);
          pv[1].p_A_light_delta_flag = false;
          pv[1].russian_roulette_light = static_cast<R>(1.0);

          scene.camera.add_contribution_img_e(scene.area_lights[light_id].get_emission() * pv[0].alpha * get_weight(pv,0,pv.size()),y,x,thread_id);

        }
      }

      Vec3 next_dir;
      bool flag = false;
      R p_sigma = static_cast<R>(1.0);
      R rev_russian_roulette_probability = static_cast<R>(1.0);
      R cos_ = static_cast<R>(1.0);
      Material_info rev_material_info = material_info;
      // with update alpha and russian_roulette_probability

      if (scene.materials[material_info.material_id]->get_BXDF_TYPE() == BT_BSDF) {
        const BSDF *bsdf = static_cast<const BSDF*>(scene.materials[material_info.material_id].get());

        if (bsdf->has_delta()) {
          flag = true;
        }

        const Vec3 &normal = material_info.normal;

        Vec3 u,v;
        Utils::generate_orthonormal_basis_from_normal(u,v,normal);

        Vec3 w_o = Vec3(vdot(u,dir),vdot(v,dir),vdot(normal,dir));

        Vec3 w_i;

        const Color f_s = bsdf->sample_f_s(material_info,w_i,w_o,xor128,p_sigma);

        cos_ = static_cast<R>(w_i[2]);

        alpha = alpha * f_s * std::abs(cos_) / p_sigma;

        next_dir = w_i[0] * u + w_i[1] * v + w_i[2] * normal;

        const R f_cos = Utils::luminance(f_s) * std::abs(cos_);

        russian_roulette_probability = std::max(static_cast<R>(max_russian_roulette_),std::min(static_cast<R>(1.0),f_cos / p_sigma));

        if (w_i[2] < static_cast<R>(0.0)) {
          rev_material_info.normal = -material_info.normal;
          rev_material_info.is_entering = !material_info.is_entering;
          w_o = -w_o;
          w_i = -w_i;
        }

        const Color rev_f_s = bsdf->f_s(rev_material_info,w_o,w_i);//相反性が成り立っていればf_sでよい
        const R rev_f_cos = Utils::luminance(rev_f_s) * std::abs(cos_o);

        rev_russian_roulette_probability = std::max(static_cast<R>(max_russian_roulette_),std::min(static_cast<R>(1.0),rev_f_cos / bsdf->pdf(rev_material_info,w_o,w_i)));

      }

#ifdef _DEBUG
      if (russian_roulette_probability < static_cast<R>(0.0) || rev_russian_roulette_probability < static_cast<R>(0.0)) {
        std::cerr << russian_roulette_probability << " warning russian roulette probability " << rev_russian_roulette_probability << std::endl;
      }
#endif

      const R rev_P = p_A_measure(scene
                                 ,rev_material_info
                                 ,next_dir
                                 ,dir
                                 ,previous_cos
                                 ,dist2);


      Vertex_data &v = spv[spv.size() - 2];

      if (is_light_tracing) {
        v.p_A_eye = rev_P;
        v.p_A_eye_delta_flag = flag;
        v.russian_roulette_eye = rev_russian_roulette_probability;
      } else {
        v.p_A_light = rev_P;
        v.p_A_light_delta_flag = flag;
        v.russian_roulette_light = rev_russian_roulette_probability;
      }

      if (xor128.rnd() > ( (spv.size() >= min_num_vertices_of_sub_path_) ? russian_roulette_probability : static_cast<R>(1.0) ) ) {
        break;
      }

      alpha = alpha / ( (spv.size() >= min_num_vertices_of_sub_path_) ? russian_roulette_probability : static_cast<R>(1.0) );

      previous_cos = cos_;
      previous_flag = flag;
      previous_p_sigma = p_sigma;

      ray.org[0] = material_info.p[0];
      ray.org[1] = material_info.p[1];
      ray.org[2] = material_info.p[2];

      ray.dir[0] = next_dir[0];
      ray.dir[1] = next_dir[1];
      ray.dir[2] = next_dir[2];

      R kFar = static_cast<R>(1.0e+30);
      ray.min_t = static_cast<R>(0.0);
      ray.max_t = kFar;
    }

  }

  // x3からx2に来た時x1を選ぶ面積確率密度
  inline R p_A_measure(const Scene &scene
                    ,const Material_info &x2_mi
                    ,const Vec3 &w_23
                    ,const Vec3 &w_21
                    ,const R cos1
                    ,const R dist_square) const {
    const BXDF *bxdf = scene.materials[x2_mi.material_id].get();

    R ret = static_cast<R>(0.0);
    if (bxdf->get_BXDF_TYPE() == BT_BSDF) {
      const BSDF *bsdf = static_cast<const BSDF*>(bxdf);

      Vec3 u,v;
      Utils::generate_orthonormal_basis_from_normal(u,v,x2_mi.normal);

      const Vec3 w_o(vdot(u,w_23),vdot(v,w_23),vdot(x2_mi.normal,w_23));
      const Vec3 w_i(vdot(u,w_21),vdot(v,w_21),vdot(x2_mi.normal,w_21));

      ret = bsdf->pdf(x2_mi,w_i,w_o) * std::abs(cos1) / dist_square;
    }

    return ret;
  }

  inline R p_A_measure_from_camera(const Scene &scene
                                  ,const R cos_0
                                  ,const R cos_1
                                  ,const R dist2) const {
    return (std::abs(cos_1) * scene.camera.get_dist_sensor_lens() * scene.camera.get_dist_sensor_lens() * scene.camera.get_pdf_i()) 
          / (dist2 * Math::pow<3>(std::abs(cos_0)));
  }

  inline R calc_russian_roulette(const Scene &scene
                                ,const Material_info &material_info
                                ,const Vec3 &omega_i
                                ,const Vec3 &omega_o
                                ,const R cos_) const {//calc russian roulette probability when you sampling for w_i direction.  cos_ = dot(n,w_i)
    const BXDF *bxdf = scene.materials[material_info.material_id].get();

    R ret = static_cast<R>(0.0);

    if (bxdf->get_BXDF_TYPE() == BT_BSDF) {
      const BSDF *bsdf = static_cast<const BSDF*>(bxdf);
      Vec3 u,v;
      Utils::generate_orthonormal_basis_from_normal(u,v,material_info.normal);

      const Vec3 w_i(vdot(u,omega_i),vdot(v,omega_i),vdot(material_info.normal,omega_i));
      const Vec3 w_o(vdot(u,omega_o),vdot(v,omega_o),vdot(material_info.normal,omega_o));

      const R f = Utils::luminance(bsdf->f_s(material_info,w_i,w_o));

      ret = f * std::abs(cos_) / bsdf->pdf(material_info,w_i,w_o);

      ret = std::max(static_cast<R>(max_russian_roulette_),std::min(static_cast<R>(1.0),ret));
    }

    return ret;
  }

  inline void merge(Scene &scene,const int y,const int x,const int thread_id) const {
    const auto &lspv = light_sub_path_vertices_[thread_id];
    const auto &espv = eye_sub_path_vertices_[thread_id];
    for (int s = 1;s <= lspv.size();s++) {
      for (int t = 1;t <= espv.size();t++) {
        const Vertex_data v_y = lspv[s - 1];
        const Vertex_data v_z = espv[t - 1];

        const Material_info &material_info_y = v_y.material_info;
        const Material_info &material_info_z = v_z.material_info;

        const BXDF *bxdf_y = scene.materials[material_info_y.material_id].get();
        const BXDF *bxdf_z = scene.materials[material_info_z.material_id].get();

        if (check_has_delta(bxdf_y) || check_has_delta(bxdf_z)) {
          continue;
        }

        const Vec3 w_y_i = (s > 1) ? nanort::vnormalize(lspv[s - 2].material_info.p - material_info_y.p) : material_info_y.normal;
        const Vec3 w_y_o = vnormalize(material_info_z.p - material_info_y.p);

        const Vec3 w_z_i = -w_y_o;
        const Vec3 w_z_o = (t > 1) ? nanort::vnormalize(espv[t - 2].material_info.p - material_info_z.p) : material_info_z.normal;

        const R cos_y = vdot(material_info_y.normal,w_y_o);
        const R cos_z = vdot(material_info_z.normal,w_z_i);

        if ((cos_y < static_cast<R>(0.0) && !check_is_transmittance(bxdf_y)) || (cos_z < static_cast<R>(0.0) && !check_is_transmittance(bxdf_z))) {
          continue;
        }

        const Color fy = calc_bxdf(bxdf_y,material_info_y,w_y_o,w_y_i);

        Color fz(static_cast<R>(0.0));

        nanort::Ray<R> shadow_ray;

        shadow_ray.org[0] = material_info_y.p[0];
        shadow_ray.org[1] = material_info_y.p[1];
        shadow_ray.org[2] = material_info_y.p[2];

        shadow_ray.dir[0] = w_y_o[0];
        shadow_ray.dir[1] = w_y_o[1];
        shadow_ray.dir[2] = w_y_o[2];

        const R kFar = static_cast<R>(1e+30);
        shadow_ray.min_t = static_cast<R>(0.0);
        shadow_ray.max_t = kFar;

        int y_dash,x_dash;

        if (t == 1) {
          Intersection<R> isect;
          R cos_sensor,cos_object_plane;

          if (!scene.camera.get_intersection_with_lens(shadow_ray,isect,cos_sensor,cos_object_plane,y_dash,x_dash)) {
            continue;
          }

          fz = Color(Math::pow<4>(cos_sensor / cos_object_plane));
        } else {
          fz = calc_bxdf(bxdf_z,material_info_z,w_z_i,w_z_o);
        }

        const R dist = vlength(material_info_y.p - material_info_z.p);
        const R dist2 = dist * dist;

        shadow_ray.max_t = dist;

        const Color c = fy * fz * std::abs(cos_y * cos_z / dist2);

        std::vector<Vertex_data> spv;
        spv.reserve(lspv.size() + espv.size());

        for(int i = 0;i < s;i++) spv.emplace_back(lspv[i]);
        for(int i = t - 1;i >= 0;i--) spv.emplace_back(espv[i]);

        if (t == 1) {
          spv[s - 1].p_A_eye = p_A_measure_from_camera(scene,cos_z,cos_y,dist2);
          spv[s - 1].p_A_eye_delta_flag = false;
          spv[s - 1].russian_roulette_eye = static_cast<R>(1.0);
        } else {
          spv[s - 1].p_A_eye = p_A_measure(scene,material_info_z,w_z_o,w_z_i,cos_y,dist2);
          spv[s - 1].p_A_eye_delta_flag = false;
          spv[s - 1].russian_roulette_eye = calc_russian_roulette(scene,material_info_z,w_z_i,w_z_o,cos_z);
        }

        spv[s].p_A_light = p_A_measure(scene,material_info_y,w_y_i,w_y_o,cos_z,dist2);
        spv[s].p_A_light_delta_flag = false;

        if (s == 1) {
          spv[s].russian_roulette_light = static_cast<R>(1.0);
        }else {
          spv[s].russian_roulette_light = calc_russian_roulette(scene,material_info_y,w_y_o,w_y_i,cos_y);
        }


        R w = get_weight(spv,s,t);

        if (std::isnan(w) || std::isinf(w)) {
          std::cerr << "warning! mis weight nan" << std::endl;
#ifdef _DEBUG
          for (auto v : spv) {
            std::cerr << v.russian_roulette_light << " " << v.russian_roulette_eye << "  ";
          }
          std::cerr << std::endl;
#endif
          w = static_cast<R>(0.0);
        }

        const SHAPE_TYPE light_shape_type = material_info_z.shape_type;
        const unsigned int light_shape_id = material_info_z.shape_id;
        const unsigned int light_prim_id = material_info_z.prim_id;

        const SHAPE_TYPE last_hit_shape_type = material_info_y.shape_type;
        const unsigned int last_hit_shape_id = material_info_y.shape_id;
        const unsigned int last_hit_prim_id = material_info_y.prim_id;

        if (Intersector::is_shadow(shadow_ray,scene,light_shape_type,light_shape_id,light_prim_id,last_hit_shape_type,last_hit_shape_id,last_hit_prim_id)) {
          continue;
        }

        if (t == 1) {
          scene.camera.add_contribution_img_l(w * v_y.alpha * c * v_z.alpha,y_dash,x_dash,thread_id);
        } else {
          scene.camera.add_contribution_img_e(w * v_y.alpha * c * v_z.alpha,y,x,thread_id);
        }
      }
    }
  }

  inline bool check_has_delta (const BXDF *bxdf) const {
    bool ret = false;
    if (bxdf->get_BXDF_TYPE() == BT_BSDF) {
      const BSDF *bsdf = static_cast<const BSDF*>(bxdf);

      ret = bsdf->has_delta();
    }

    return ret;
  }

  inline bool check_is_transmittance (const BXDF *bxdf) const {
    bool ret = false;
    if (bxdf->get_BXDF_TYPE() == BT_BSDF) {
      const BSDF *bsdf = static_cast<const BSDF*>(bxdf);
      ret = bsdf->has_btdf();
    }

    return ret;
  }

  inline Color calc_bxdf(const BXDF *bxdf,const Material_info &material_info,const Vec3 &w_i,const Vec3 &w_o) const {

    Color ret(static_cast<R>(0.0));

    if (bxdf->get_BXDF_TYPE() == BT_BSDF) {
      const BSDF *bsdf = static_cast<const BSDF*>(bxdf);

      Vec3 u,v;
      Utils::generate_orthonormal_basis_from_normal(u,v,material_info.normal);

      const Vec3 omega_i(vdot(u,w_i),vdot(v,w_i),vdot(material_info.normal,w_i));
      const Vec3 omega_o(vdot(u,w_o),vdot(v,w_o),vdot(material_info.normal,w_o));

      ret = bsdf->f_s(material_info,omega_i,omega_o);

    }

    return ret;
  }

  inline R get_weight(const std::vector<Vertex_data> &spv,const int s,const int t) const {
    const int k = static_cast<int>(spv.size()) - 1;

#ifdef _DEBUG
    for (int i = 0;i <= k;i++) {
      if (spv[i].p_A_light < static_cast<R>(-1.0) || spv[i].russian_roulette_light < static_cast<R>(-1.0) || spv[i].p_A_eye < static_cast<R>(-1.0) || spv[i].russian_roulette_eye < static_cast<R>(-1.0)) {
        std::cerr << "warning no update probability" << std::endl;
      }
    }
#endif

    R w = static_cast<R>(0.0);

    R m = static_cast<R>(1.0);

    int delta_cou = 0;
    for (int i = s - 1;i >= 0;i--) {
      //const R Pi = spv[i].p_A_light * ( (i >= min_num_vertices_of_sub_path_) ? spv[i].russian_roulette_light : static_cast<R>(1.0) ) / spv[i].p_A_eye / ( (k - i >= min_num_vertices_of_sub_path_) ? spv[i].russian_roulette_eye : static_cast<R>(1.0) );
      const R Pi =  spv[i].p_A_eye * ( (k - i >= min_num_vertices_of_sub_path_) ? spv[i].russian_roulette_eye : static_cast<R>(1.0) ) / (spv[i].p_A_light * ( (i >= min_num_vertices_of_sub_path_) ? spv[i].russian_roulette_light : static_cast<R>(1.0) ) );

      m *= Pi;

      if (spv[i].p_A_light_delta_flag) {
        delta_cou++;
      } 
      if (spv[i].p_A_eye_delta_flag) {
        delta_cou--;
      }

      if (delta_cou == 0) {
        w += m * m;
      }
#ifdef _DEBUG
      else if(delta_cou < 0) {
        std::cerr << "warning A in func get_weight" << std::endl;
      }
#endif
    }

    m = static_cast<R>(1.0);
    delta_cou = 0;

#ifdef T_EQUAL_0
    for (int i = s;i <= k;i++){ 
#else
    for (int i = s;i <= k - 1;i++){ 
#endif
      const R Pi = spv[i].p_A_light * ( (i >= min_num_vertices_of_sub_path_) ? spv[i].russian_roulette_light : static_cast<R>(1.0) ) / ( spv[i].p_A_eye * ( (k - i >= min_num_vertices_of_sub_path_) ? spv[i].russian_roulette_eye : static_cast<R>(1.0) ) );

      m *= Pi;

      if (spv[i].p_A_light_delta_flag) {
        delta_cou--;
      } 
      if(spv[i].p_A_eye_delta_flag) {
        delta_cou++;
      }

      if (delta_cou == 0) {
        w += m * m;
      }
#ifdef _DEBUG
      else if(delta_cou < 0) {
        std::cerr << "warning B in func get_weight" << std::endl;
      }
#endif
    }

    w += static_cast<R>(1.0);

    return static_cast<R>(1.0) / w;
  }

private:

  std::vector<Xor128> xor128s_;
  mutable std::vector<std::vector<Vertex_data>> light_sub_path_vertices_;
  mutable std::vector<std::vector<Vertex_data>> eye_sub_path_vertices_;

  unsigned int light_material_id_;
  unsigned int eye_material_id_;

  R max_russian_roulette_ = 0.1;

  R min_num_vertices_of_sub_path_ = 5;

};
}
