#pragma once

#include "Core.hpp"
#include "Scene.hpp"

#include <iostream>

namespace ridaisai2018 {

class Intergrator {
public:
  Intergrator();
  virtual ~Intergrator();

protected:

  inline void update_material_info(Material_info &material_info,const Scene &scene,const nanort::Ray<R> &ray,const Intersection<R> &isect) const {
    if (isect.shape_type == ST_MESH) {
      const auto mesh = scene.meshes[isect.shape_id];

      mesh.get_Material_info(material_info,isect);

      material_info.p = Vec3(ray.org) + Vec3(ray.dir) * isect.t;

    }

#ifdef _DEBUG
    else {
      std::cerr << "warning in func update_material_info" << std::endl;
    }
#endif

  }

  inline void update_light_info(Light_info &light_info,const Scene &scene,const nanort::Ray<R> &ray,const Intersection<R> &isect) const {
    if (isect.shape_type == ST_MESH) {
      const auto mesh = scene.meshes[isect.shape_id];

      mesh.get_Light_info(light_info,isect);

      light_info.p = Vec3(ray.org) + Vec3(ray.dir) * isect.t;

    }

#ifdef _DEBUG
    else {
      std::cerr << "warning in func update_light_info" << std::endl;
    }
#endif

  }

  inline void update_last_hit(SHAPE_TYPE &shape_type,unsigned int &shape_id,unsigned int &prim_id,const Intersection<R> &isect) const {

    if (isect.shape_type == ST_MESH) {
      shape_type = ST_MESH;
      shape_id = isect.shape_id;
      prim_id = isect.prim_id;
    }

#ifdef _DEBUG
    else {
      std::cerr << "warning in func update_last_hit" << std::endl;
    }
#endif

  }
};
}
