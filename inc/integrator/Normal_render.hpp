#pragma once

#include "Core.hpp"
#include "Scene.hpp"
#include "Intersector.hpp"
#include "Integrator.hpp"

#include <omp.h>

namespace ridaisai2018 {

class Normal_render : public Intergrator {
public:
  inline bool rendering(Scene &scene,int samples = 1) const {
    const int threads = scene.camera.get_threads();

#pragma omp parallel for schedule(dynamic, 1) num_threads(threads)
    for (int y = 0;y < scene.camera.get_pixel_h();y++) {
      for (int x = 0;x < scene.camera.get_pixel_w();x++) {
        for (int k = 0;k < samples;k++) {
          const int thread_id = omp_get_thread_num();

          Vec3 z0;Vec3 normal_z0;R pdf_A_z0;Color alpha_E_1;
          Vec3 dir_to_z1;R pdf_sigma_to_z1;Color alpha_E_2;
          R cos_;

          scene.camera.sample_z0_and_next_direction(y,x,thread_id
              ,z0,normal_z0,pdf_A_z0,alpha_E_1
              ,dir_to_z1,pdf_sigma_to_z1,alpha_E_2
              ,cos_);

          nanort::Ray<R> ray;

          ray.org[0] = z0[0];
          ray.org[1] = z0[1];
          ray.org[2] = z0[2];

          ray.dir[0] = dir_to_z1[0];
          ray.dir[1] = dir_to_z1[1];
          ray.dir[2] = dir_to_z1[2];

          R kFar = 1.0e+30f;
          ray.min_t = 0.0f;
          ray.max_t = kFar;

          Intersection isect;  

          bool hit = Intersector::all_intersector(ray,scene,isect);

          Color rgb(static_cast<R>(0));

          if (hit) {

            Material_info material_info;

            if (isect.shape_type == ST_MESH) {
              const auto mesh = scene.meshes[isect.shape_id];

              mesh.get_Material_info(material_info,isect);

              material_info.p = Vec3(ray.org) + Vec3(ray.dir) * isect.t;
            }
#ifdef _DEBUG
            else {
              std::cerr << "found unknown SHAPE TYPE in Normal Render" << std::endl;
            }
#endif

            rgb[0] = material_info.normal[0] * static_cast<R>(0.5) + static_cast<R>(0.5);
            rgb[1] = material_info.normal[1] * static_cast<R>(0.5) + static_cast<R>(0.5);
            rgb[2] = material_info.normal[2] * static_cast<R>(0.5) + static_cast<R>(0.5);

          }

          scene.camera.add_contribution_img_e(rgb,y,x,thread_id);
          scene.camera.add_count_e(y,x,thread_id);

        }
      }
    }
    return true;
  }
};
}
