#pragma once

#include "Core.hpp"
#include "Scene.hpp"
#include "Intersector.hpp"
#include "Light_sampling.hpp"
#include "Integrator.hpp"
#include "Xor128.hpp"

#include "BSDF.hpp"

#include <vector>
#include <omp.h>

namespace ridaisai2018 {

class Pathtracer_NEE : public Intergrator {
public:
  Pathtracer_NEE() = delete;
  Pathtracer_NEE(const Scene &scene);
  ~Pathtracer_NEE();

  void init(const Scene &scene);

  inline bool rendering(Scene &scene,int samples = 1) const {
    const int threads = scene.camera.get_threads();

#pragma omp parallel for schedule(dynamic, 1) num_threads(threads)
    for (int y = 0;y < scene.camera.get_pixel_h();y++) {
      for (int x = 0;x < scene.camera.get_pixel_w();x++) {
        for (int k = 0;k < samples;k++) {
          const int thread_id = omp_get_thread_num();

          Vec3 z0;Vec3 normal_z0;R pdf_A_z0;Color alpha_E_1;
          Vec3 dir_to_z1;R pdf_sigma_to_z1;Color alpha_E_2;
          R cos_;

          scene.camera.sample_z0_and_next_direction(y,x,thread_id
              ,z0,normal_z0,pdf_A_z0,alpha_E_1
              ,dir_to_z1,pdf_sigma_to_z1,alpha_E_2
              ,cos_);

          nanort::Ray<R> ray;

          ray.org[0] = z0[0];
          ray.org[1] = z0[1];
          ray.org[2] = z0[2];

          ray.dir[0] = dir_to_z1[0];
          ray.dir[1] = dir_to_z1[1];
          ray.dir[2] = dir_to_z1[2];

          R kFar = static_cast<R>(1.0e+30);
          ray.min_t = static_cast<R>(0.0);
          ray.max_t = kFar;

          Color alpha = alpha_E_2;

          Color contribution(static_cast<R>(0.0));

          SHAPE_TYPE last_hit_shape_type = ST_SHAPE;
          unsigned int last_hit_shape_id = -1;
          unsigned int last_hit_prim_id = -1;

          bool flag = true;

          while (true) {

            Intersection isect;

            bool hit = Intersector::all_intersector(ray,scene,isect,last_hit_shape_type,last_hit_shape_id,last_hit_prim_id);

            if(!hit) {
              contribution += alpha * static_cast<R>(0.0);
              break;
            }

            Material_info material_info;

            update_material_info(material_info,scene,ray,isect);

            Light_info light_info;

            update_light_info(light_info,scene,ray,isect);

            update_last_hit(last_hit_shape_type,last_hit_shape_id,last_hit_prim_id,isect);

            R cos_o = nanort::vdot(-Vec3(ray.dir) , material_info.normal);
      
            if (cos_o < static_cast<R>(0.0)) {
              material_info.normal = -material_info.normal;
              material_info.is_entering = false;
              cos_o = -cos_o;
            }

            if(flag && light_info.light_type == LT_AREA_LIGHT && vdot(Vec3(ray.dir) , light_info.normal) < static_cast<R>(0.0)) contribution += alpha * scene.area_lights[light_info.light_id].get_emission();

            Vec3 next_dir;
            if (scene.materials[material_info.material_id]->get_BXDF_TYPE() == BT_BSDF) {
              BSDF *bsdf = static_cast<BSDF*>(scene.materials[material_info.material_id].get());

              const Vec3 dir = vnormalize(-Vec3(ray.dir)); 

              if (vdot(dir , material_info.normal) < static_cast<R>(0.0)) {
                material_info.normal = -material_info.normal;
              }

              const Vec3 &normal = material_info.normal;

    			    Vec3 u,v;

              Utils::generate_orthonormal_basis_from_normal(u,v,normal);

              const Vec3 w_o = Vec3(vdot(u,dir),vdot(v,dir),vdot(normal,dir));

              if(bsdf->has_delta()) {
                flag = true;;
              } else {
                Light_info light_info;
                Light_sampling::all_light_sampling(scene,xor128s[thread_id],light_info);

                const Vec3 path = light_info.p - material_info.p;

                const Vec3 w_l = nanort::vnormalize(path);

                const R d = nanort::vlength(path);

                SHAPE_TYPE light_shape_type = (light_info.light_type == LT_AREA_LIGHT) ? light_info.shape_type : ST_SHAPE;
                unsigned int light_shape_id = (light_info.light_type == LT_AREA_LIGHT) ? light_info.shape_id : -1;
                unsigned int light_prim_id = (light_info.light_type == LT_AREA_LIGHT) ? light_info.prim_id : -1;

                nanort::Ray<R> shadow_ray;

                shadow_ray.org[0] = material_info.p[0];
                shadow_ray.org[1] = material_info.p[1];
                shadow_ray.org[2] = material_info.p[2];

                shadow_ray.dir[0] = w_l[0];
                shadow_ray.dir[1] = w_l[1];
                shadow_ray.dir[2] = w_l[2];

                shadow_ray.min_t = static_cast<R>(0.0);
                shadow_ray.max_t = d;

                Color emission;

                if (light_info.light_type == LT_AREA_LIGHT) {
                  emission = scene.area_lights[light_info.light_id].get_emission();
                }

                const R wl_dot_nl = nanort::vdot(-w_l,light_info.normal);
                const R wl_dot_np = nanort::vdot(w_l,normal);

                // hack
                //
                //contribution = Color(wl_dot_nl > static_cast<R>(0.0));
                //contribution = Color(wl_dot_np > static_cast<R>(0.0));
                //contribution = Color(!Intersector::is_shadow(shadow_ray,scene,light_shape_type,light_shape_id,light_prim_id,last_hit_shape_type,last_hit_shape_id,last_hit_prim_id));
                //break;


                if (wl_dot_nl > static_cast<R>(0.0)
                 && wl_dot_np > static_cast<R>(0.0)
                 && !Intersector::is_shadow(shadow_ray,scene,light_shape_type,light_shape_id,light_prim_id,last_hit_shape_type,last_hit_shape_id,last_hit_prim_id)) {

                  const Vec3 w_i = Vec3(vdot(u,w_l),vdot(v,w_l),vdot(normal,w_l));
                  contribution += alpha * bsdf->f_s(material_info,w_i,w_o) * emission * std::abs(wl_dot_nl) * std::abs(wl_dot_np) / d / d / light_info.pdf_A;
                } 
                
                flag = false;
              }

              Vec3 w_i;
              R pdf;

              const Color f_s = bsdf->sample_f_s(material_info,w_i,w_o,xor128s[thread_id],pdf);

              alpha = alpha * f_s * std::abs(w_i[2]) / pdf;

              next_dir = w_i[0] * u + w_i[1] * v + w_i[2] * normal;

            }

            R rrp = 0.90;

            if (rrp < xor128s[thread_id].rnd()) {
              break;
            }

            alpha = alpha / rrp;

            ray.org[0] = material_info.p[0];
            ray.org[1] = material_info.p[1];
            ray.org[2] = material_info.p[2];

            ray.dir[0] = next_dir[0];
            ray.dir[1] = next_dir[1];
            ray.dir[2] = next_dir[2];

            R kFar = static_cast<R>(1.0e+30);
            ray.min_t = static_cast<R>(0.0);
            ray.max_t = kFar;

          }

          scene.camera.add_contribution_img_e(contribution,y,x,thread_id);
          scene.camera.add_count_e(y,x,thread_id);

        }
      }
    }
    return true;
  }
private:

  std::vector<Xor128> xor128s;
};
}
