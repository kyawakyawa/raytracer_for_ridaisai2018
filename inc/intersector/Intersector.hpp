#pragma once

#include "Core.hpp"
#include "Scene.hpp"

#include "Triangle_intersector.hpp"

#include<vector>

namespace ridaisai2018 {

  namespace Intersector {

    template<class Shape,class Intersector,SHAPE_TYPE ST> 
    inline bool intersector(nanort::Ray<R> ray,const std::vector<nanort::BVHAccel<R>> &accels,const std::vector<Shape> &shapes,Intersection<R> &intersection,const unsigned int last_hit_shape_id = -1,const unsigned int last_hit_prim_id = -1) {
      bool something_hit = false;

      for (int i = 0;i < shapes.size();i++) {
        const auto &shape = shapes[i];

        const int accel_id = shape.get_accel_id();

#ifdef _DEBUG
        if(accel_id < 0 || accels.size() <= accel_id) {
           std::cerr << "warning in Intersector accel_id:" << accel_id << std::endl;
         }
#endif

        const auto &accel = accels[accel_id];

        Intersector intersector(shape);
  
        Intersection<R> isect;

        nanort::BVHTraceOptions bvh_trace_options;

        bvh_trace_options.skip_prim_id = (i == last_hit_shape_id) ? last_hit_prim_id : -1;

        bool hit = accel.Traverse(ray,intersector,&isect,bvh_trace_options);

        if (hit) {
          something_hit = true;

          ray.max_t = isect.t;

          intersection = isect;

          intersection.shape_type = ST;
          intersection.shape_id = i;
        }
      }

      return something_hit;
    }


    inline bool all_intersector(nanort::Ray<R> ray,const Scene &scene,Intersection<R> &intersection,const SHAPE_TYPE last_hit_shape_type = ST_SHAPE,const unsigned int last_hit_shape_id = -1,const unsigned int last_hit_prim_id = -1) {
      const auto &meshes = scene.meshes;
      const auto &accels = scene.accels;

      bool something_hit = false;

      unsigned int lhsi = (last_hit_shape_type == ST_MESH) ? last_hit_shape_id : -1;
      unsigned int lhpi = (last_hit_shape_type == ST_MESH) ? last_hit_prim_id : -1;

      something_hit |= intersector<Mesh,Triangle_intersector<Intersection<R>>,ST_MESH>(ray,accels,meshes,intersection,lhsi,lhpi);

      return something_hit;
    }

    template<class Shape,class Intersector,SHAPE_TYPE ST> 
    inline bool is_shadow(nanort::Ray<R> ray,const std::vector<nanort::BVHAccel<R>> &accels,const std::vector<Shape> &shapes,const unsigned int light_shape_id = -1,const unsigned int light_prim_id = -1,const unsigned int last_hit_shape_id = -1,const unsigned int last_hit_prim_id = -1) {

      for (int i = 0;i < shapes.size();i++) {
        const auto &shape = shapes[i];

        const int accel_id = shape.get_accel_id();

#ifdef _DEBUG
        if(accel_id < 0 || accels.size() <= accel_id) {
           std::cerr << "warning in Intersector accel_id:" << accel_id << std::endl;
         }
#endif

        const auto &accel = accels[accel_id];

        Intersector intersector(shape);
  
        Intersection<R> isect;

        nanort::BVHTraceOptions bvh_trace_options;

        bvh_trace_options.skip_prim_id = (i == last_hit_shape_id) ? last_hit_prim_id : -1;

        bool hit = accel.Traverse(ray,intersector,&isect,bvh_trace_options);

        if (hit && (light_shape_id != i || light_prim_id != isect.prim_id) ) {
          return true;
        }
      }

      return false;
 
    }

    inline bool is_shadow(nanort::Ray<R> ray,const Scene &scene,const SHAPE_TYPE light_shape_type = ST_SHAPE,const unsigned int light_shape_id = -1,const unsigned int light_prim_id = -1,const SHAPE_TYPE last_hit_shape_type = ST_SHAPE,const unsigned int last_hit_shape_id = -1,const unsigned int last_hit_prim_id = -1) {
      const auto &meshes = scene.meshes;
      const auto &accels = scene.accels;

      unsigned int lsi = (light_shape_type == ST_MESH) ? light_shape_id : -1;
      unsigned int lpi = (light_shape_type == ST_MESH) ? light_prim_id : -1;

      unsigned int lhsi = (last_hit_shape_type == ST_MESH) ? last_hit_shape_id : -1;
      unsigned int lhpi = (last_hit_shape_type == ST_MESH) ? last_hit_prim_id : -1;

      if(is_shadow<Mesh,Triangle_intersector<Intersection<R>>,ST_MESH>(ray,accels,meshes,lsi,lpi,lhsi,lhpi)) return true;

      return false;
    }
  } 

}
