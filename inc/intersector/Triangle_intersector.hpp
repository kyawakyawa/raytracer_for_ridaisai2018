#pragma once

#include "Core.hpp"
#include "Mesh.hpp"

namespace ridaisai2018 {

template<class H = Intersection<R>>
class Triangle_intersector : public nanort::TriangleIntersector<R,H> {
public:
  Triangle_intersector(const Mesh &mesh) : nanort::TriangleIntersector<R,H>(mesh.get_vertices(),mesh.get_vertice_index(),sizeof(R) * 3){}
};

}
