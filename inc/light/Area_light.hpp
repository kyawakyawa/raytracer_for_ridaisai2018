#pragma once

#include "Light.hpp"

namespace ridaisai2018 {

class Area_light : public Light {

public:
  Area_light() = delete;

  Area_light(const Vec3 emission,const SHAPE_TYPE shape_type,const unsigned int shape_id);

  inline SHAPE_TYPE get_shape_type() const {
    return shape_type_;
  }

  inline unsigned int get_shape_id() const {
    return shape_id_;
  }

  inline Color get_emission() const {
    return emission_;
  }

  ~Area_light(){};

private:

  Color emission_;
  SHAPE_TYPE shape_type_;
  unsigned int shape_id_;

};

}
