#pragma once
#include "Core.hpp"
#include "Scene.hpp"

namespace ridaisai2018 {

  namespace Light_sampling {

  inline void all_light_sampling(const Scene &scene,const Xor128 &xor128,Light_info &light_info) {
    const R u1 = xor128.rnd();

    LIGHT_TYPE light_type = LT_NO_LIGHT;
    for (int i = 1;i < LT_NO_LIGHT + 1;i++) {
      if (u1 < scene.light_type_F[i]) {
        light_type = static_cast<LIGHT_TYPE>(i - 1);
        break;
      }

#ifdef _DEBUG
      if(i == LT_NO_LIGHT - 1) {
        std::cerr << "warning in func all_light_sampling" << std::endl;
      }
#endif
    }

    light_info.light_type = light_type;

    if (light_type == LT_AREA_LIGHT) {
      const R u2 = xor128.rnd();

      int area_light_id = 0;

      for (int i = 1;i < scene.light_F[LT_AREA_LIGHT].size();i++) {
        if (u2 < scene.light_F[LT_AREA_LIGHT][i]) {
          area_light_id = i - 1;
          break;
        }
#ifdef _DEBUG
        if(i == static_cast<int>(scene.light_F[LT_AREA_LIGHT].size()) - 1) {
          std::cerr << "warning in func all_light_sampling" << std::endl;
        }
#endif
      }

      light_info.light_id = area_light_id;

      const auto &area_light = scene.area_lights[area_light_id];

      SHAPE_TYPE shape_type = area_light.get_shape_type();
      unsigned int shape_id = area_light.get_shape_id();

      light_info.shape_type = shape_type;
      light_info.shape_id = shape_id;

      if (shape_type == ST_MESH) {
        scene.meshes[shape_id].sample_one_point(xor128,light_info.p,light_info.pdf_A,light_info.normal,light_info.prim_id);
      }

#ifdef _DEBUG
      else {
        std::cerr << "warning in func all_light_sampling" << std::endl;
      }
#endif

      light_info.pdf_A *= scene.light_F[LT_AREA_LIGHT][area_light_id + 1] - scene.light_F[LT_AREA_LIGHT][area_light_id];

    }

#ifdef _DEBUG
    else {
      std::cerr << "warning in func all_light_sampling" << std::endl;
    }
#endif

    light_info.pdf_A *= scene.light_type_F[light_type + static_cast<LIGHT_TYPE>(1)] - scene.light_type_F[light_type];

  }

  }
}
