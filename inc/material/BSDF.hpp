#pragma once

#include "Core.hpp"
#include "Xor128.hpp"
#include "BXDF.hpp"

namespace ridaisai2018 {

class BSDF : public BXDF {
public:
  BSDF(){}
  virtual ~BSDF(){}

  inline BXDF_TYPE get_BXDF_TYPE() const {
    return BT_BSDF;
  }

  inline virtual bool has_delta() const = 0;

  inline virtual bool has_brdf() const = 0;
  inline virtual bool has_btdf() const = 0;

  inline virtual Color f_s(const Material_info &material_info,const Vec3 &w_i,const Vec3 &w_o) const = 0;
  inline virtual Color sample_f_s(const Material_info &material_info,Vec3 &w_i,const Vec3 &w_o,const Xor128 &xor128,R &pdf) const = 0;
  inline virtual R pdf(const Material_info &material_info,const Vec3 &w_i,const Vec3 &w_o) const = 0;

//  inline R Fresnel_Schlick(const Material_info &material_info,const Vec3 &w_o) {
//
//  }

};

}
