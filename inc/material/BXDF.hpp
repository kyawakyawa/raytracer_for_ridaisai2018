#pragma once

#include "Core.hpp"

namespace ridaisai2018 {

class BXDF {

public:
  BXDF(){};
  virtual ~BXDF(){};

  inline virtual BXDF_TYPE get_BXDF_TYPE() const = 0;
  inline virtual bool has_delta() const = 0; // has delta の時はfやpdfはdeltaの係数を返す

};

} 
