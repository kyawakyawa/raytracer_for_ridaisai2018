#pragma once

#include "Core.hpp"
#include "BSDF.hpp"

#include <iostream>
#include <cmath>

namespace ridaisai2018 {

class Ideal_reflaction : public BSDF {
public:

  Ideal_reflaction() : albedo_(static_cast<R>(0.75)) , n_(static_cast<R>(1.55)) {}
  Ideal_reflaction(const R albedo, const R n = static_cast<R>(1.55)) : albedo_(albedo) , n_(n) {}
  Ideal_reflaction(const R *albedo ,const R n = static_cast<R>(1.55)) : albedo_(albedo) , n_(n){}
  Ideal_reflaction(const Color &albedo, const R n = static_cast<R>(1.55)) : albedo_(albedo) , n_(n){}

  inline bool has_delta() const {
    return true;
  }

  inline bool has_brdf() const {
    return true;
  }

  inline bool has_btdf() const {
    return true;
  }

  inline Color f_s(const Material_info &material_info,const Vec3 &w_i,const Vec3 &w_o) const {
#ifdef _DEBUG
    if (w_o[2] < static_cast<R>(0.0)) {
      std::cerr << "warning in Ideal_reflaction f_r" << std::endl;
    }
#endif
    const R n = material_info.is_entering ? static_cast<R>(1.0) / n_ : n_;
    const R dn = -w_o[2];
    const R cos2t = static_cast<R>(1.0) - n * n * (static_cast<R>(1.0) - dn * dn);

    if (cos2t <= static_cast<R>(0.0)) {
      return albedo_ / std::abs(w_i[2]);
    }

    const Vec3 &&T  = nanort::vnormalize(-n * w_o - (n * dn + std::sqrt(cos2t) ) * Vec3(static_cast<R>(0.0),static_cast<R>(0.0),static_cast<R>(1.0)));
    const R a = static_cast<R>(1.0) - n_,b = static_cast<R>(1.0) + n_;
    const R F0 = (a * a) / (b * b);

    const R Fr = F0 + (static_cast<R>(1.0) - F0) * Math::pow<5>(static_cast<R>(1.0) + (material_info.is_entering ? dn : T[2]));
    const R Tr = (static_cast<R>(1.0) - Fr) * n * n;

#ifdef _DEBUG
    if (Fr < static_cast<R>(0.0) || static_cast<R>(1.0) < Fr) {
      std::cerr << "warning in Ideal Refraction f_s" << std::endl;
    }
#endif

    if (w_i[2] > static_cast<R>(0.0)) {
      //BRDF
      return albedo_ * Fr / std::abs(w_i[2]);
    }
    //BTDF
    return albedo_ * Tr / std::abs(w_i[2]);
  }

  inline Color sample_f_s(const Material_info &material_info,Vec3 &w_i,const Vec3 &w_o,const Xor128 &xor128,R &pdf) const {
    const R n = material_info.is_entering ? static_cast<R>(1.0) / n_ : n_;
    const R dn = -w_o[2];
    const R cos2t = static_cast<R>(1.0) - n * n * (static_cast<R>(1.0) - dn * dn);

    const Vec3 &&r = static_cast<R>(2.0) * w_o[2] * Vec3(static_cast<R>(0.0),static_cast<R>(0.0),static_cast<R>(1.0)) - w_o;

    if (cos2t <= static_cast<R>(0.0)) {
      pdf = static_cast<R>(1.0);
      w_i = r;
      return albedo_ / std::abs(w_i[2]);
    }

    const Vec3 &&T  = nanort::vnormalize(-n * w_o - (n * dn + std::sqrt(cos2t) ) * Vec3(static_cast<R>(0.0),static_cast<R>(0.0),static_cast<R>(1.0)));
    const R a = static_cast<R>(1.0) - n_,b = static_cast<R>(1.0) + n_;
    const R F0 = (a * a) / (b * b);

    const R Fr = F0 + (static_cast<R>(1.0) - F0) * Math::pow<5>(static_cast<R>(1.0) + (material_info.is_entering ? dn : T[2]));
    const R Tr = (static_cast<R>(1.0) - Fr) * n * n;

    const R probability = static_cast<R>(0.25) + static_cast<R>(0.5) * Fr;
    //const R probability = Fr;
    //const R probability = static_cast<R>(0.5);

#ifdef _DEBUG
    if (Fr < static_cast<R>(0.0) || static_cast<R>(1.0) < Fr) {
      std::cerr << "warning in Ideal Refraction sample" << std::endl;
    }
#endif

   if (xor128.rnd() < probability) {
      pdf = probability;
      w_i = r;
      return albedo_ * Fr / std::abs(w_i[2]);
    }

    pdf = static_cast<R>(1.0) - probability;
    w_i = T;
    return albedo_ * Tr / std::abs(w_i[2]);
  }

  inline R pdf(const Material_info &material_info,const Vec3 &w_i,const Vec3 &w_o) const {
#ifdef _DEBUG
    if (w_o[2] < static_cast<R>(0.0)) {
      std::cerr << "warning in Ideal_reflaction pdf" << std::endl;
    }
#endif
    const R n = material_info.is_entering ? static_cast<R>(1.0) / n_ : n_;
    const R dn = -w_o[2];
    const R cos2t = static_cast<R>(1.0) - n * n * (static_cast<R>(1.0) - dn * dn);

    if (cos2t <= static_cast<R>(0.0)) {
      return static_cast<R>(1.0);
    }

    const Vec3 &&T  = nanort::vnormalize(-n * w_o - (n * dn + std::sqrt(cos2t) ) * Vec3(static_cast<R>(0.0),static_cast<R>(0.0),static_cast<R>(1.0)));
    const R a = static_cast<R>(1.0) - n_,b = static_cast<R>(1.0) + n_;
    const R F0 = (a * a) / (b * b);

    const R Fr = F0 + (static_cast<R>(1.0) - F0) * Math::pow<5>(static_cast<R>(1.0) + (material_info.is_entering ? dn : T[2]));

#ifdef _DEBUG
    if (Fr < static_cast<R>(0.0) || static_cast<R>(1.0) < Fr) {
      std::cerr << "warning in Ideal Refraction pdf" << std::endl;
    }
#endif

    const R probability = static_cast<R>(0.25) + static_cast<R>(0.5) * Fr;
    //const R probability = Fr;
    //const R probability = static_cast<R>(0.5);

    if (w_i[2] > static_cast<R>(0.0)) {
      //BRDF
      return probability;
    }
    //BTDF
    return static_cast<R>(1.0) - probability;
  }

private:
  Color albedo_;
  R n_;
};
}
