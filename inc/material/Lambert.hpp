#pragma once

#include "Core.hpp"
#include "BSDF.hpp"

#include <iostream>
#include <cmath>

namespace ridaisai2018 {

class Lambert : public BSDF {
public:

  Lambert() : albedo_(0.75) {}
  Lambert(const R albedo) : albedo_(albedo){}
  Lambert(const R *albedo) : albedo_(albedo){}
  Lambert(const Color &albedo) : albedo_(albedo){}

  inline bool has_delta() const {
    return false;
  }

  inline bool has_brdf() const {
    return true;
  }

  inline bool has_btdf() const {
    return false;
  }

  inline Color f_s(const Material_info &material_info,const Vec3 &w_i,const Vec3 &w_o) const {
    if (w_i[2] < static_cast<R>(0.0) || w_o[2] < static_cast<R>(0.0)) {
#ifdef _DEBUG
      std::cerr << "warning in Lambert f_r" << std::endl;
#endif
      return static_cast<R>(0.0);
    }
    return albedo_ / Constant::kPI;
  }

  inline Color sample_f_s(const Material_info &material_info,Vec3 &w_i,const Vec3 &w_o,const Xor128 &xor128,R &pdf) const {

#ifdef _DEBUG
    if (w_o[2] < static_cast<R>(0.0)) {
      std::cerr << "warning in Lambert" << std::endl;
    }
#endif

    R u1 = xor128.rnd() * static_cast<R>(2.0) * Constant::kPI,u2 = xor128.rnd(),u3 = std::sqrt(u2);

    w_i = Vec3(std::cos(u1) * u3,std::sin(u1) * u3,std::sqrt(static_cast<R>(1.0) - u2));

    pdf = w_i[2] / Constant::kPI;

    return albedo_ / Constant::kPI;
  }

  inline R pdf(const Material_info &material_info,const Vec3 &w_i,const Vec3 &w_o) const {
    if (w_i[2] < static_cast<R>(0.0) || w_o[2] < static_cast<R>(0.0)) {
#ifdef _DEBUG
      std::cerr << "warning in Lambert pdf" << std::endl;
#endif
      return static_cast<R>(0.0);
    }
    return w_i[2] / Constant::kPI;
  }

private:
  Color albedo_;
};
}
