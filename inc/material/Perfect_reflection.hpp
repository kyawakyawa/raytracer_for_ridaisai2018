#pragma once

#include "Core.hpp"
#include "BSDF.hpp"

#include <iostream>
#include <cmath>

namespace ridaisai2018 {

class Perfect_reflection : public BSDF {
public:

  Perfect_reflection() : albedo_(0.75) {}
  Perfect_reflection(const R albedo) : albedo_(albedo){}
  Perfect_reflection(const R *albedo) : albedo_(albedo){}
  Perfect_reflection(const Color &albedo) : albedo_(albedo){}

  inline bool has_delta() const {
    return true;
  }

  inline bool has_brdf() const {
    return true;
  }

  inline bool has_btdf() const {
    return false;
  }

  inline Color f_s(const Material_info &material_info,const Vec3 &w_i,const Vec3 &w_o) const {
    if (w_i[2] < static_cast<R>(0.0) || w_o[2] < static_cast<R>(0.0)) {
#ifdef _DEBUG
      std::cerr << "warning in Perfect_reflection f_r" << std::endl;
#endif
      return static_cast<R>(0.0);
    }
    return albedo_ / w_i[2];
  }

  inline Color sample_f_s(const Material_info &material_info,Vec3 &w_i,const Vec3 &w_o,const Xor128 &xor128,R &pdf) const {
    w_i = static_cast<R>(2.0) * w_o[2] * Vec3(static_cast<R>(0.0),static_cast<R>(0.0),static_cast<R>(1.0)) - w_o;

    pdf = static_cast<R>(1.0);

    return albedo_ / w_i[2];
  }

  inline R pdf(const Material_info &material_info,const Vec3 &w_i,const Vec3 &w_o) const {
    if (w_i[2] < static_cast<R>(0.0) || w_o[2] < static_cast<R>(0.0)) {
#ifdef _DEBUG
      std::cerr << "warning in Perfect_reflection pdf" << std::endl;
#endif
      return static_cast<R>(0.0);
    }
    return static_cast<R>(1.0);
  }

private:
  Color albedo_;
};
}
