#pragma once

#include "Core.hpp"
#include "Scene.hpp"

namespace ridaisai2018 {
  namespace Build_accel {

    void build_all_BVH(Scene &scene); 

  }
}
