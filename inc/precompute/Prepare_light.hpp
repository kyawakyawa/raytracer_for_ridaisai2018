#pragma once

#include "Core.hpp"
#include "Scene.hpp"

namespace ridaisai2018 {
  namespace Prepare_light {

    void prepare_all_light(Scene &scene); 

  }
}
