#pragma once

#include <climits>

#include "Core.hpp"

namespace ridaisai2018 {

class Xor128 {

public:

  Xor128(){};
  ~Xor128(){};

  inline unsigned long long  xor128(void) const {
  // x, y, z, w が乱数のシード
      unsigned long long t;
      t=(x^(x<<11));x=y;y=z;z=w; return( w=(w^(w>>19))^(t^(t>>8)) );
  }

  inline R rnd () const {
  	unsigned long long n = xor128();

  	R M = (R)ULLONG_MAX;
  	M += 1.0;
  	return (R)n / M;
  }

private:


  mutable unsigned long long x=123456789,y=362436069,z=521288629,w=88675123;

};

}

