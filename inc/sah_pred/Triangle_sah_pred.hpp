#pragma once

#include "Core.hpp"
#include "Mesh.hpp"

namespace ridaisai2018 {

class Triangle_sah_pred : public nanort::TriangleSAHPred<R> {

public:
  Triangle_sah_pred(const Mesh &mesh) : nanort::TriangleSAHPred<R>(mesh.get_vertices(),mesh.get_vertice_index(),sizeof(R) * 3) {}
};

}
