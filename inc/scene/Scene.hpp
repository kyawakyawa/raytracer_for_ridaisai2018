#pragma once

#include "Core.hpp"
#include "Thin_lens_camera.hpp"
#include "Shape.hpp"
#include "Mesh.hpp"

#include "BXDF.hpp"

#include "Area_light.hpp"

#include <memory>
#include <vector>

namespace ridaisai2018 {

struct Scene {
  Scene(){}
  ~Scene(){}

  Thin_lens_camera camera;

  std::vector<Mesh> meshes;

  std::vector<nanort::BVHAccel<R>> accels;

  std::vector<std::shared_ptr<BXDF>> materials;

  std::vector<Area_light> area_lights;

  R light_type_F[LT_NO_LIGHT + 1]; // ライトの種類に関する累積分布関数

  std::vector<R> light_F[LT_NO_LIGHT]; // それぞれのライトについてどれを選ぶかについての累積分布関数
};

}
