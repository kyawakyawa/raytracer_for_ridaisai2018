#pragma once

#include "Core.hpp"
#include "Xor128.hpp"
#include "Shape.hpp"
#include "Lambert.hpp"
#include "tiny_obj_loader.h"

#include <iostream>
#include <vector>
#include <string>
#include <memory>

namespace ridaisai2018 {

class Mesh : Shape {

public:

  Mesh();
  Mesh(const std::string &name
      ,const int num_vertices,const int num_faces
      ,const std::shared_ptr<R[]> vertices,const std::shared_ptr<R[]> normals,const std::shared_ptr<R[]> uvs
      ,const std::shared_ptr<unsigned int[]> vertice_index,const std::shared_ptr<unsigned int[]> normal_index,const std::shared_ptr<unsigned int[]> uv_index
      ,const std::shared_ptr<unsigned int[]> material_ids,const int accel_id = -1);

  ~Mesh();

  static inline bool load_obj(std::vector<Mesh> &meshes,std::vector<std::shared_ptr<BXDF>> &materials,const std::string &filename,const bool is_split = true) {

    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> mtls;

    std::string err;

    bool ret = tinyobj::LoadObj(&attrib, &shapes, &mtls, &err, filename.c_str());

    if (!err.empty()) {
      std::cerr << err << std::endl;
    }

    if (!ret) {
      return false;
    }

    std::shared_ptr<R[]> vertices {new R[attrib.vertices.size()]};
    std::shared_ptr<R[]> normals {new R[attrib.normals.size()]};
    std::shared_ptr<R[]> uvs {new R[attrib.texcoords.size()]};

    for (int i = 0;i < attrib.vertices.size();i++) {
      vertices[i] = static_cast<R>(attrib.vertices[i]);
    }

    for (int i = 0;i < attrib.normals.size();i++) {
      normals[i] = static_cast<R>(attrib.normals[i]);
    }

    for (int i = 0;i < attrib.texcoords.size();i++) {
      uvs[i] = static_cast<R>(attrib.texcoords[i]);
    }

    const int material_offset = materials.size();

    for (const auto &mtl : mtls) {
      materials.emplace_back(new Lambert(Vec3(mtl.diffuse[0],mtl.diffuse[1],mtl.diffuse[2])));
    }

    if (is_split) {
      meshes.reserve(shapes.size());

      for (int i = 0;i < shapes.size();i++) {
        auto shape = shapes[i];

        std::cerr << "Load [" << shape.name << "]" << std::endl;

        const std::string name = shape.name;

        for(auto v : shape.mesh.num_face_vertices) {
          if (v != 3) {
            std::cerr << "Sorry! You can use only triangle!" << std::endl;
            return 0;
          }
        }

        std::shared_ptr<unsigned int[]> vertice_index {new unsigned int[shape.mesh.indices.size()]};
        std::shared_ptr<unsigned int[]> normal_index{new unsigned int[shape.mesh.indices.size()]};
        std::shared_ptr<unsigned int[]> uv_index {new unsigned int[shape.mesh.indices.size()]};

        for (int i = 0;i < shape.mesh.indices.size();i++) {
          vertice_index[i] = shape.mesh.indices[i].vertex_index;
          normal_index[i] = shape.mesh.indices[i].normal_index;
          uv_index[i] = shape.mesh.indices[i].texcoord_index;
        }

        std::shared_ptr<unsigned int[]> material_ids {new unsigned int[shape.mesh.material_ids.size()]};

        for (int i = 0;i < shape.mesh.material_ids.size();i++) {
          material_ids[i] = shape.mesh.material_ids[i] + material_offset;
        }

        meshes.emplace_back( name
                           ,attrib.vertices.size(),shape.mesh.num_face_vertices.size()
                           ,vertices,normals,uvs
                           ,vertice_index,normal_index,uv_index,material_ids);
      }
    } else {
      std::cerr << "Sorry! I have not implemented uniting obj file yet" << std::endl;
      return false;
    }

    return true;
    
  }

 inline void get_Material_info(Material_info &material_info,const Intersection<R> &isect) const {

#ifdef _DEBUG
    if (isect.prim_id < 0 || isect.prim_id >= num_faces_) {
      std::cerr << "warning in Mesh::get_Material_info" << std::endl;
    }
#endif

    material_info.u = isect.u;
    material_info.v = isect.v;

    const int p_id = isect.prim_id;
    const Vec3 p0(&vertices_[3 * vertice_index_[3 * p_id + 0]]);
    const Vec3 p1(&vertices_[3 * vertice_index_[3 * p_id + 1]]);
    const Vec3 p2(&vertices_[3 * vertice_index_[3 * p_id + 2]]);
    material_info.normal = vnormalize(nanort::vcross(p1 - p0 ,p2 - p1));

    material_info.shape_type = isect.shape_type;
    material_info.shape_id = isect.shape_id;
    material_info.prim_id = isect.prim_id;
    
    material_info.material_id = material_ids_[isect.prim_id];

  }

 inline void get_Light_info(Light_info &light_info,const Intersection<R> &isect) const {

#ifdef _DEBUG
    if (isect.prim_id < 0 || isect.prim_id >= num_faces_) {
      std::cerr << "warning in Mesh::get_Material_info" << std::endl;
    }
#endif
    const int p_id = isect.prim_id;

    light_info.pdf_A = static_cast<R>(1.0) / face_S_[p_id] / static_cast<R>(num_faces_);

    const Vec3 p0(&vertices_[3 * vertice_index_[3 * p_id + 0]]);
    const Vec3 p1(&vertices_[3 * vertice_index_[3 * p_id + 1]]);
    const Vec3 p2(&vertices_[3 * vertice_index_[3 * p_id + 2]]);
    light_info.normal = vnormalize(nanort::vcross(p1 - p0 ,p2 - p1));

    light_info.light_type = (light_ids_[p_id] == -1) ? LT_NO_LIGHT : LT_AREA_LIGHT;
    light_info.light_id = light_ids_[p_id];

  }

  inline void sample_one_point(const Xor128 &xor128,Vec3 &point,R &pdf_A,Vec3 &normal,unsigned int &prim_id) const {
    prim_id = static_cast<unsigned int> (xor128.rnd() * static_cast<R>(num_faces_));

    prim_id = std::min<R>(num_faces_ - 1,std::max<R>(0,prim_id));

    const R u1 = xor128.rnd();
    const R u2 = xor128.rnd();

    const R M = (u1 > u2) ? u1 : u2;
    const R m = (u1 < u2) ? u1 : u2;

    const Vec3 p0(&vertices_[3 * vertice_index_[3 * prim_id + 0]]);
    const Vec3 p1(&vertices_[3 * vertice_index_[3 * prim_id + 1]]);
    const Vec3 p2(&vertices_[3 * vertice_index_[3 * prim_id + 2]]);
    
    point = p0 * m + p1 * (static_cast<R>(1.0) - M) + p2 * (M - m);

    pdf_A = static_cast<R>(1.0) / face_S_[prim_id] / static_cast<R>(num_faces_);

    normal = vnormalize(nanort::vcross(p1 - p0 ,p2 - p1));
  }

  inline std::string get_name() const {
    return name_;
  }

  inline int get_num_prim() const {
    return num_faces_;
  }

  inline R* get_vertices() const {
    return vertices_.get();
  }

  inline unsigned int* get_vertice_index() const {
    return vertice_index_.get();
  }

  inline void set_accel_id(const int accel_id) {
#ifdef _DEBUG
    if(accel_id < 0) {
      std::cerr << "warning accel_id is negative" << std::endl;
    }
#endif

    accel_id_ = accel_id;
  }

  inline int get_accel_id() const {
    return accel_id_;
  }

  inline R get_S() const {
    return S_;
  }

  inline R get_prim_S(const unsigned int prim_id) const {
    return face_S_[prim_id];
  }

  void set_material(const unsigned int material_id,const unsigned int prim_id = -1);
  void set_light(const unsigned int light_id,const unsigned int prim_id = -1);
 
private:

  std::string name_;

  int num_vertices_;
  int num_faces_;

  std::shared_ptr<R[]> vertices_;
  std::shared_ptr<R[]> normals_;
  std::shared_ptr<R[]> uvs_;

  std::shared_ptr<unsigned int[]> vertice_index_;
  std::shared_ptr<unsigned int[]> normal_index_;
  std::shared_ptr<unsigned int[]> uv_index_;

  std::shared_ptr<unsigned int[]> material_ids_;

  int accel_id_;

  R S_;
  std::shared_ptr<R[]> face_S_;

  std::shared_ptr<unsigned int[]> light_ids_;

};

}
