#include "ImageIO.hpp"
#include "stb_image.h"
#include "stb_image_write.h"

#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <cmath>

namespace ridaisai2018 {

ImageIO::ImageIO() {
  width_ = 0;
  height_ = 0;
  channel_ = 3;
}

ImageIO::ImageIO(const std::vector<Color> &img,const int width,const int height,const int channel) {
  set_image(img,width,height,channel);
}

bool ImageIO::set_image(const std::vector<Color> &img,const int width,const int height,const int channel) {
  if(width <= 0 || height <= 0 || img.size() != width * height) {
    std::cerr << "failed set image data! The data is broken!" << std::endl;
    return false;
  }

  width_ = width;
  height_ = height;

  img_.clear();
  img_.resize(width_ * height_);
  std::copy(img.begin(),img.end(),img_.begin());

  channel_ = channel;

  return true;
}

bool ImageIO::load(const std::string &filename) {
  int n;

  float *pixelf = stbi_loadf(filename.c_str(),&width_,&height_,&n,0);

  if(pixelf == nullptr) {
    return false;
  }

  img_.clear();
  img_.reserve(width_ *  height_);

  for (int i = 0;i < width_ * height_;i++) {
    img_.emplace_back(static_cast<R>(pixelf[i * n]),static_cast<R>(pixelf[i * n + 1]),static_cast<R>(pixelf[i * n + 2]));
  }

  stbi_image_free(pixelf);

  return true;
}

bool ImageIO::write_png(const std::string &filename,const bool gamma_correction_on) const {
  if(filename.length() <= 4 || filename[filename.length() - 4] != '.' || filename[filename.length() - 3] != 'p' || filename[filename.length() - 2] != 'n' || filename[filename.length() - 1] != 'g') {
    std::cerr << "failed writing png image [" << filename << "]! filename is invalid! " << std::endl;
    return false;
  }

  if (width_ <= 0 || height_ <= 0 || img_.size() != width_ * height_) {
    std::cerr << "failed writing png image [" << filename << "]! Image data is broken!" << std::endl;
    return false;
  }

  std::vector<Color> img_sRGB;

  if (gamma_correction_on)linerTosRGB(img_sRGB);
  else {
    img_sRGB.resize(img_.size());
    std::copy(img_.begin(),img_.end(),img_sRGB.begin());
  }

  int n = channel_;

  std::vector<unsigned char> pixels(width_ * height_ * n);

  for (int i = 0;i < width_ * height_;i++) {
    pixels.at(i * n + 0) = static_cast<unsigned char>( std::max(static_cast<R>(0.0),std::min(static_cast<R>(255.0),(img_sRGB.at(i))[0] * static_cast<R>(256.0))) );
    pixels.at(i * n + 1) = static_cast<unsigned char>( std::max(static_cast<R>(0.0),std::min(static_cast<R>(255.0),(img_sRGB.at(i))[1] * static_cast<R>(256.0))) );
    pixels.at(i * n + 2) = static_cast<unsigned char>( std::max(static_cast<R>(0.0),std::min(static_cast<R>(255.0),(img_sRGB.at(i))[2] * static_cast<R>(256.0))) );

  }

  int ret = stbi_write_png(filename.c_str(),width_,height_,n,pixels.data(),width_ * n);

  if (ret == 0) {
    std::cerr << "failed writing png image [" << filename << "]! stb_image_write Error!" << std::endl;
    return false;
  }
  
  return true;
}

bool ImageIO::write_hdr(const std::string &filename) const {
  if(filename.length() <= 4 || filename[filename.length() - 4] != '.' || filename[filename.length() - 3] != 'h' || filename[filename.length() - 2] != 'd' || filename[filename.length() - 1] != 'r') {
    std::cerr << "failed writing hdr image [" << filename << "]! filename is invalid! " << std::endl;
    return false;
  }

  if (width_ <= 0 || height_ <= 0 || img_.size() != width_ * height_) {
    std::cerr << "failed writing hdr image [" << filename << "]! Image data is broken!" << std::endl;
    return false;
  }

  FILE *fp = fopen(filename.c_str(), "wb" );

  if (fp == NULL) {
    std::cerr << "faled writing hdr image" << std::endl;
    return false;
  }

  unsigned char ret = 0x0a;

  fprintf(fp, "#?RADIANCE%c", (unsigned char)ret);
  fprintf(fp, "# Made with 100%% pure HDR Shop%c", ret);
  fprintf(fp, "FORMAT=32-bit_rle_rgbe%c", ret);
  fprintf(fp, "EXPOSURE=1.0000000000000%c%c", ret, ret);

  fprintf(fp, "-Y %d +X %d%c", height_, width_, ret);
	for (int i = 0; i < height_; i++) {
    std::vector<unsigned char> line;
    line.reserve(width_ * 4);
    for (int j = 0; j < width_; j ++) {

      Color color = img_.at(i * width_ + j);

      double d = std::max(color[0],std::max(color[1],color[2]));

      if (d <= 1e-32) {
        line.push_back(static_cast<unsigned char>(0));
        line.push_back(static_cast<unsigned char>(0));
        line.push_back(static_cast<unsigned char>(0));
        line.push_back(static_cast<unsigned char>(0));
      }else {
        int e ;
        double m = std::frexp(d, &e);
        d = m * 256.0 / d;

        line.push_back(static_cast<unsigned char>(color[0] * d));
        line.push_back(static_cast<unsigned char>(color[1] * d));
        line.push_back(static_cast<unsigned char>(color[2] * d));
        line.push_back(static_cast<unsigned char>(e + 128));
      }
		}
		fprintf(fp, "%c%c", 0x02, 0x02);
		fprintf(fp, "%c%c", (width_ >> 8) & 0xFF, width_ & 0xFF);
		for (int i = 0; i < 4; i ++) {
			for (int cursor = 0; cursor < width_;) {
				const int cursor_move = std::min(127, width_ - cursor);
				fprintf(fp, "%c", cursor_move);
				for (int j = cursor;  j < cursor + cursor_move; j ++)
          fprintf(fp,"%c",line[j * 4 + i]);
				cursor += cursor_move;
			}
		}
	}

	fclose(fp);

  return true;
}

void ImageIO::linerTosRGB(std::vector<Color> &img) const {
  constexpr R a = static_cast<R>(0.055);
  constexpr R a1 = static_cast<R>(1.055);
  constexpr R p = static_cast<R>(1.0 / 2.4);

  img.resize(img_.size());

  std::copy(img_.begin(),img_.end(),img.begin());

  for(auto &pixel : img) {
    for (int i = 0; i < 3; i++) {
      if (pixel[i] <= static_cast<R>(0.0031308)) {
        pixel[i] = pixel[i] * static_cast<R>(12.92);
      } else {
        pixel[i] = a1 * std::pow(pixel[i],p) - a;
      }
    }
  }
}

ImageIO::~ImageIO() {

}

}
