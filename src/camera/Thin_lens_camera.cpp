#include "Core.hpp"
#include "Thin_lens_camera.hpp"

namespace ridaisai2018 {

Thin_lens_camera::Thin_lens_camera() : Thin_lens_camera(1
                                                      ,Vec3(0,0,0),Vec3(0,0,-1),Vec3(0,0,1),2 * std::atan(2.0 / 3) * 180.0 / 3.1415926535
                                                      ,480,480
                                                      ,140,42.0/17) {}

Thin_lens_camera::Thin_lens_camera(const int threads
                                  ,const Vec3 &position,const Vec3 &dir,const Vec3 &up,const R fov
                                  ,const int pixel_w,const int pixel_h
                                  ,const R focus_distance,const R f_number) {
  init(threads
      ,position,dir,up,fov
      ,pixel_w,pixel_h
      ,focus_distance,f_number);
}

void Thin_lens_camera::init(const int threads
                           ,const Vec3 &position,const Vec3 & dir,const Vec3 &up,const R fov
                           ,const int pixel_w,const int pixel_h
                           ,const R focus_distance,const R f_number) {

  threads_ = threads;

  position_ = position;
  dir_ = dir;
  up_ = up;

  pixel_w_ = pixel_w;
  pixel_h_ = pixel_h;

  sample_count_l_.clear();
  sample_count_l_.resize(threads_,0);

  sample_count_e_.clear();
  sample_count_e_.resize(threads_,std::vector<int>(pixel_w_ * pixel_h_,0));

  img_l_.clear();
  img_l_.resize(threads_,std::vector<Color>(pixel_w_ * pixel_h_,static_cast<R>(0.0)));

  img_e_.clear();
  img_e_.resize(threads_,std::vector<Color>(pixel_w_ * pixel_h_,static_cast<R>(0.0)));

  x_ = nanort::vnormalize(nanort::vcross(dir,up));
  y_ = nanort::vnormalize(nanort::vcross(dir,x_));

  object_plane_x_ = -x_;
  object_plane_y_ = -y_;

  width_of_sensor_ = static_cast<R>(3.0);
  height_of_sensor_ = width_of_sensor_ * static_cast<R>(pixel_h_) / static_cast<R>(pixel_w_);
  dist_sensor_lens_ = static_cast<R>(0.5) * width_of_sensor_ / std::tan(fov * Constant::kPI / static_cast<R>(360.0));
  sensor_center_ = -dir * dist_sensor_lens_ + position_;

  focal_length_ = focus_distance * dist_sensor_lens_ / (focus_distance + dist_sensor_lens_);
  lens_radius_ = focal_length_ / f_number * static_cast<R>(0.5);
  f_number_ = f_number;

  width_of_object_plane_ = width_of_sensor_ * focus_distance / dist_sensor_lens_;
  height_of_object_plane_ = height_of_sensor_ * focus_distance / dist_sensor_lens_;
  dist_lens_object_plane_ = focus_distance;
  object_center_ = dist_lens_object_plane_ * dir + position_;

  pdf_i_ = static_cast<R>(1.0f) / (height_of_sensor_ * width_of_sensor_ / static_cast<R>(pixel_w_ * pixel_h_));
  pdf_l_ = static_cast<R>(1.0f) / Constant::kPI / lens_radius_ / lens_radius_;
  sensibility_ = static_cast<R>(1.0) * f_number * f_number * pdf_i_;

  for (int i = 0;i < threads_;i++) {
    xor128s_.emplace_back();
  }
}

Thin_lens_camera::~Thin_lens_camera() {

}

}
