#include "Core.hpp"
#include "Lambert.hpp"
#include "Bdpt.hpp"

#include <vector>

namespace ridaisai2018 {

Bdpt::Bdpt(Scene &scene) {
  init(scene);
}

void Bdpt::init(Scene &scene) {
  xor128s_.clear();
  xor128s_.resize(scene.camera.get_threads());

  light_sub_path_vertices_.clear();
  light_sub_path_vertices_.resize(scene.camera.get_threads());

  eye_sub_path_vertices_.clear();
  eye_sub_path_vertices_.resize(scene.camera.get_threads());

  scene.materials.emplace_back(new Lambert(Color(Constant::kPI)));

  light_material_id_ = scene.materials.size() - 1;

  scene.materials.emplace_back(new Lambert(Color(static_cast<R>(1.0))));

  eye_material_id_ = scene.materials.size() - 1;
}

Bdpt::~Bdpt() {

}

}
