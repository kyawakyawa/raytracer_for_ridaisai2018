#include "Core.hpp"
#include "Pathtracer_NEE.hpp"

namespace ridaisai2018 {

Pathtracer_NEE::Pathtracer_NEE(const Scene &scene) {
  init(scene);
}

void Pathtracer_NEE::init(const Scene &scene) {
  xor128s.clear();
  xor128s.resize(scene.camera.get_threads());
}

Pathtracer_NEE::~Pathtracer_NEE() {

}

}
