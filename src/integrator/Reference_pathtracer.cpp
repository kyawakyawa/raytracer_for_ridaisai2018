#include "Core.hpp"
#include "Reference_pathtracer.hpp"

namespace ridaisai2018 {

Reference_pathtracer::Reference_pathtracer(const Scene &scene) {
  init(scene);
}

void Reference_pathtracer::init(const Scene &scene) {
  xor128s.clear();
  xor128s.resize(scene.camera.get_threads());
}

Reference_pathtracer::~Reference_pathtracer() {

}

}
