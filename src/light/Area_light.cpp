#include "Area_light.hpp"

namespace ridaisai2018 {

  Area_light::Area_light(const Vec3 emission,const SHAPE_TYPE shape_type,const unsigned int shape_id) {
  emission_ = emission;
  shape_type_ = shape_type;
  shape_id_ = shape_id;
}

}
