#include <iostream>
#include <vector>
#include <ctime>

#include "Core.hpp"
#include "ImageIO.hpp"
#include "Scene.hpp"
#include "Build_accel.hpp"
#include "Prepare_light.hpp"

#include "Lambert.hpp"
#include "Perfect_reflection.hpp"
#include "Ideal_reflaction.hpp"

#include "Normal_render.hpp"
#include "Reference_pathtracer.hpp"
#include "Pathtracer_NEE.hpp"
#include "Bdpt.hpp"

using namespace ridaisai2018;

int main() {

  Scene scene;

  bool ret = Mesh::load_obj(scene.meshes,scene.materials,"cornellbox_suzanne_lucy.obj");

  //scene.meshes[8].emission = Vec3(static_cast<R>(15));
  scene.materials.emplace_back(new Ideal_reflaction(static_cast<R>(0.99)));
  scene.meshes[1].set_material(scene.materials.size() - 1);
  //scene.materials.emplace_back(new Perfect_reflection(static_cast<R>(0.99)));
  //scene.materials.emplace_back(new Lambert(Color(static_cast<R>(0.25),static_cast<R>(0.25),static_cast<R>(0.75))));
  //scene.meshes[4].set_material(scene.materials.size() - 1);

  scene.area_lights.emplace_back(Vec3(static_cast<R>(15)),ST_MESH,8);

  //bool ret = Mesh::load_obj(scene.meshes,scene.materials,"sceneRabbit.obj");

  //scene.materials.emplace_back(new Ideal_reflaction(static_cast<R>(0.99)));
  //scene.meshes[1].set_material(scene.materials.size() - 1);

  //scene.area_lights.emplace_back(Vec3(static_cast<R>(15)),ST_MESH,3);

  Prepare_light::prepare_all_light(scene);

  assert(ret);

  int num_threds = 4;
  int samples = 32;

  int w = 512;
  int h = 512;

  scene.camera = Thin_lens_camera(num_threds
                                 ,Vec3(0.0,5.0,20.0),Vec3(0.0,0.0,-1.0),Vec3(0.0,1.0,0.0),60
                                 ,w,h
                                 ,21.5,100);

  //scene.camera = Thin_lens_camera(num_threds
  //                               ,Vec3(2.841,2.865,-5.561),Vec3(0.0,0.0,1.0),Vec3(0.0,1.0,0.0),60
  //                               ,w,h
  //                               ,5,100);

  Build_accel::build_all_BVH(scene);

  std::cerr << "BVH build finished" << std::endl;



  //Normal_render render;
  //render.rendering(scene,samples);

  //Reference_pathtracer render(scene);
  //render.rendering(scene,samples);

  //Pathtracer_NEE render(scene);
  //render.rendering(scene,samples);

  Bdpt render(scene);
  render.rendering(scene,samples);


  std::vector<Color> rgb;

  scene.camera.out_rgb(rgb);

  ImageIO image_io(rgb,w,h);

  time_t t = time(NULL);

  image_io.write_png("/tmp/" + std::to_string(t) + ".png");

  return 0;
}
