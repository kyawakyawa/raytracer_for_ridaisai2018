#include <iostream>
#include <vector>

#include "Core.hpp"
#include "ImageIO.hpp"
#include "Mesh.hpp"

using namespace ridaisai2018;

int main() {

  std::vector<Mesh> meshes;

  bool ret = Mesh::load_obj(meshes,"cornellbox_suzanne_lucy.obj");

  if (!ret) {
    return 0;
  }

  std::vector<nanort::BVHAccel<R>> accels(meshes.size());

  for(int i = 0;i < accels.size();i++) {
    auto &accel = accels[i];
    auto &mesh = meshes[i];

    nanort::BVHBuildOptions<R> build_options;
    build_options.cache_bbox = false;

    nanort::TriangleMesh<R> triangle_mesh(mesh.get_vertices(),mesh.get_vertice_index(),sizeof(R)  * 3);
    nanort::TriangleSAHPred<R> triangle_pred(mesh.get_vertices(),mesh.get_vertice_index(),sizeof(R)  * 3);

    bool ret = accel.Build(mesh.get_num_prim(),triangle_mesh,triangle_pred,build_options);

    assert(ret);
  }
  std::cerr << "finished build BVH" << std::endl;

  const int width = 512;
  const int height = 512;

  std::vector<Color> rgb(width * height,0.0);

  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      // Simple camera. change eye pos and direction fit to .obj model.

      nanort::Ray<R> ray;
      ray.org[0] = 0.0f;
      ray.org[1] = 5.0f;
      ray.org[2] = 20.0f;

      Vec3 dir;
      dir[0] = (x / (R)width) - 0.5f;
      dir[1] = (y / (R)height) - 0.5f;
      dir[2] = -1.0f;
      dir = nanort::vnormalize(dir);
      ray.dir[0] = dir[0];
      ray.dir[1] = dir[1];
      ray.dir[2] = dir[2];

      R kFar = 1.0e+30f;
      ray.min_t = 0.0f;
      ray.max_t = kFar;

      nanort::TriangleIntersection<R> isect_nearest;
      bool hit_nearest = false;

      int mesh_id = -1;

      for (int i = 0;i < accels.size();i++) {

        auto &mesh = meshes[i];
        auto &accel = accels[i];

        nanort::TriangleIntersector<R> triangle_intersector(
            mesh.get_vertices(), mesh.get_vertice_index(), sizeof(R) * 3);
        nanort::TriangleIntersection<R> isect;
        bool hit = accel.Traverse(ray, triangle_intersector, &isect);
        if (hit) {
          // Write your shader here.
          //Vec3 normal(0.0f, 0.0f, 0.0f);
          //unsigned int fid = isect.prim_id;
          //if (mesh.facevarying_normals) {
          //  normal[0] = mesh.facevarying_normals[9 * fid + 0];
          //  normal[1] = mesh.facevarying_normals[9 * fid + 1];
          //  normal[2] = mesh.facevarying_normals[9 * fid + 2];
          //}
          //// Flip Y
          //rgb[3 * ((height - y - 1) * width + x) + 0] = fabsf(normal[0]);
          //rgb[3 * ((height - y - 1) * width + x) + 1] = fabsf(normal[1]);
          //rgb[3 * ((height - y - 1) * width + x) + 2] = fabsf(normal[2]);
          //;
          
          //rgb[(height - y - 1) * width + x][0] = (i + 1 + (isect.prim_id / mesh.get_num_faces_)) / static_cast<R>(accels.size());

          ray.max_t = isect.t;
          isect_nearest = isect;
          mesh_id = i;
          hit_nearest = true;
        }

      }

      if(hit_nearest) {
        //rgb[(height - y - 1) * width + x][0] = (mesh_id + 1 + (static_cast<R>(isect_nearest.prim_id) + 1) / meshes[mesh_id].get_num_faces_()) / static_cast<R>(accels.size());
        rgb[(height - y - 1) * width + x][0] = (static_cast<R>(isect_nearest.prim_id) + 1) / meshes[mesh_id].get_num_prim();
      }

    }
  }

  ImageIO image_io(rgb,width,height);

  image_io.write_png("test_mesh.png");

  return 0;
}
