#include <iostream>
#include <vector>
#include <ctime>

#include "Core.hpp"
#include "ImageIO.hpp"
#include "Scene.hpp"
#include "Build_accel.hpp"
#include "Normal_render.hpp"

using namespace ridaisai2018;

int main() {

  Scene scene;

  bool ret = Mesh::load_obj(scene.meshes,"cornellbox_suzanne_lucy.obj");

  assert(ret);

  int num_threds = 4;
  int samples = 32;

  int w = 512;
  int h = 512;

  scene.camera = Thin_lens_camera(num_threds
                                 ,Vec3(0.0,5.0,20.0),Vec3(0.0,0.0,-1.0),Vec3(0.0,1.0,0.0),60
                                 ,w,h
                                 ,10.0,10);

  Build_accel::build_all_BVH(scene);

  std::cerr << "BVH build finished" << std::endl;


  Normal_render render;

  render.rendering(scene,samples);

  std::vector<Color> rgb;

  scene.camera.out_rgb(rgb);

  ImageIO image_io(rgb,w,h);

  time_t t = time(NULL);

  image_io.write_png(std::to_string(t) + ".png");

  return 0;
}
