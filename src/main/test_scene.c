#include <iostream>
#include <vector>

#include "Core.hpp"
#include "ImageIO.hpp"
#include "Scene.hpp"
#include "Build_accel.hpp"
#include "Intersector.hpp"

using namespace ridaisai2018;

int main() {

  Scene scene;

  bool ret = Mesh::load_obj(scene.meshes,"cornellbox_suzanne_lucy.obj");

  assert(ret);

  int num_threds = 1;

  int w = 512;
  int h = 512;

  scene.camera = Thin_lens_camera(num_threds
                                 ,Vec3(0.0,5.0,20.0),Vec3(0.0,0.0,-1.0),Vec3(0.0,1.0,0.0),60
                                 ,w,h
                                 ,10.0,100);

  Build_accel::build_all_BVH(scene);

  std::cerr << "BVH build finished" << std::endl;

  std::vector<Color> rgb(w * h,0.0);

  for (int y = 0;y < h;y++) {
    for (int x = 0;x < w;x++) {
      Vec3 z0;Vec3 normal_z0;R pdf_A_z0;Color alpha_E_1;
      Vec3 dir_to_z1;R pdf_sigma_to_z1;Color alpha_E_2;
      R cos_;

      scene.camera.sample_z0_and_next_direction(y,x,0
                                               ,z0,normal_z0,pdf_A_z0,alpha_E_1
                                               ,dir_to_z1,pdf_sigma_to_z1,alpha_E_2
                                               ,cos_);

      nanort::Ray<R> ray;

      ray.org[0] = z0[0];
      ray.org[1] = z0[1];
      ray.org[2] = z0[2];

      ray.dir[0] = dir_to_z1[0];
      ray.dir[1] = dir_to_z1[1];
      ray.dir[2] = dir_to_z1[2];

      //ray.org[0] = 0.0f;
      //ray.org[1] = 5.0f;
      //ray.org[2] = 20.0f;

      //Vec3 dir;
      //dir[0] = (x / (R)w) - 0.5f;
      //dir[1] = (y / (R)h) - 0.5f;
      //dir[2] = -1.0f;
      //dir = nanort::vnormalize(dir);
      //ray.dir[0] = dir[0];
      //ray.dir[1] = dir[1];
      //ray.dir[2] = dir[2];


      R kFar = 1.0e+30f;
      ray.min_t = 0.0f;
      ray.max_t = kFar;

      Intersection isect;  

      bool hit = Intersector::all_intersector(ray,scene,isect);

      if (hit) {
        rgb[(h - y - 1) * w + (w - x - 1)][0] = (static_cast<R>(isect.prim_id) + 1) / scene.meshes[isect.shape_id].get_num_prim();
      }
    }
  }

  ImageIO image_io(rgb,w,h);

  image_io.write_png("test_mesh.png");

  return 0;
}
