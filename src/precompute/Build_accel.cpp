#include "Core.hpp"
#include "Scene.hpp"
#include "Build_accel.hpp"
#include "Triangle_bbox.hpp"
#include "Triangle_sah_pred.hpp"

#include <cstdio>
#include <vector>

namespace ridaisai2018 {
  namespace Build_accel {

    template<class Shape,class BBox,class SAHPred>
    void build_BVH(std::vector<nanort::BVHAccel<R>> &accels,std::vector<Shape> &shapes) {

      for (auto &shape : shapes) {
        accels.emplace_back();

        auto &accel = accels.back();

        nanort::BVHBuildOptions<R> build_options;
        build_options.cache_bbox = false;

        BBox bbox(shape);
        SAHPred sah_pred(shape);

        bool ret = accel.Build(shape.get_num_prim(),bbox,sah_pred,build_options);

        printf("BVH build [%s]\n",shape.get_name().c_str());

        assert(ret);

        shape.set_accel_id(accels.size() - 1);


        nanort::BVHBuildStatistics stats = accel.GetStatistics();
        printf("  BVH statistics:\n");
        printf("    # of leaf   nodes: %d\n", stats.num_leaf_nodes);
        printf("    # of branch nodes: %d\n", stats.num_branch_nodes);
        printf("  Max tree depth     : %d\n", stats.max_tree_depth);
        R bmin[3], bmax[3];
        accel.BoundingBox(bmin, bmax);
        printf("  Bmin               : %f, %f, %f\n", bmin[0], bmin[1], bmin[2]);
        printf("  Bmax               : %f, %f, %f\n", bmax[0], bmax[1], bmax[2]);

      }
    }

    void build_all_BVH(Scene &scene) {

      auto &accels = scene.accels;
      auto &meshes = scene.meshes;

      build_BVH<Mesh,Triangle_bbox,Triangle_sah_pred>(accels,meshes);
    }

  }
}
