#include "Core.hpp"
#include "Scene.hpp"
#include "Area_light.hpp"
#include "Prepare_light.hpp"

#include <cstdio>
#include <vector>

namespace ridaisai2018 {
  namespace Prepare_light {

    void prepare_all_light(Scene &scene) {

      // set light_id

      // Area Light
      for (int i = 0;i < scene.area_lights.size();i++) {
        auto &area_light = scene.area_lights[i];

        if (area_light.get_shape_type() == ST_MESH) {
          scene.meshes[area_light.get_shape_id()].set_light(i);
        }

      }


      // calc probabily

      {

        scene.light_type_F[0] = static_cast<R>(0.0);

        scene.light_F[LT_AREA_LIGHT].clear();
        scene.light_F[LT_AREA_LIGHT].emplace_back(static_cast<R>(0.0));

        scene.light_type_F[LT_AREA_LIGHT + 1] = static_cast<R>(0.0);

        for (int i = 0;i < scene.area_lights.size();i++) {
          auto &area_light = scene.area_lights[i];

          if (area_light.get_shape_type() == ST_MESH) {
            scene.light_F[LT_AREA_LIGHT].emplace_back(scene.meshes[area_light.get_shape_id()].get_S() * Utils::luminance(area_light.get_emission()));
            scene.light_type_F[LT_AREA_LIGHT + 1] += scene.light_F[LT_AREA_LIGHT].back();
          }

        }

        for (int i = 0;i < static_cast<int>(scene.light_F[LT_AREA_LIGHT].size()) - 1;i++) {
          scene.light_F[LT_AREA_LIGHT][i + 1] += scene.light_F[LT_AREA_LIGHT][i];
        }

        const R div = scene.light_F[LT_AREA_LIGHT].back();

        for (auto &v : scene.light_F[LT_AREA_LIGHT]) {
          v /= div;
        }

      }

      for (int i = 0;i < LT_NO_LIGHT + 1 - 1;i++) {
        scene.light_type_F[i + 1] += scene.light_type_F[i];
      }

      const R div = scene.light_type_F[LT_NO_LIGHT + 1 - 1];

      for (int i = 0;i < LT_NO_LIGHT + 1;i++) {
        scene.light_type_F[i] /= div;
      }

    }

  }
}
