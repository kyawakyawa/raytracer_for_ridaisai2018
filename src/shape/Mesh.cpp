#include "Core.hpp"
#include "Mesh.hpp"

#include <vector>
#include <string>

namespace ridaisai2018 {

Mesh::Mesh() {

}

Mesh::Mesh(const std::string &name
          ,const int num_vertices,const int num_faces
          ,const std::shared_ptr<R[]> vertices,const std::shared_ptr<R[]> normals,const std::shared_ptr<R[]> uvs
          ,const std::shared_ptr<unsigned int[]> vertice_index,const std::shared_ptr<unsigned int[]> normal_index,const std::shared_ptr<unsigned int[]> uv_index
          ,const std::shared_ptr<unsigned int[]> material_ids,const int accel_id) {

  name_ = name;

  num_vertices_ = num_vertices;
  num_faces_ = num_faces;

  vertices_ = vertices;
  normals_ = normals;
  uvs_ =uvs;

  vertice_index_ = vertice_index;
  normal_index_ = normal_index;
  uv_index_ = uv_index;

  accel_id_ = accel_id;

  material_ids_ = material_ids;

  S_ = static_cast<R>(0.0);//面積計算
  face_S_ = std::shared_ptr<R[]>(new R[num_faces_]);

  for (int i = 0;i < num_faces_;i++) {
    const Vec3 p0(&vertices_[3 * vertice_index_[3 * i + 0]]);
    const Vec3 p1(&vertices_[3 * vertice_index_[3 * i + 1]]);
    const Vec3 p2(&vertices_[3 * vertice_index_[3 * i + 2]]);

    face_S_[i] = vlength(vcross(p2 - p0,p1 - p0)) * static_cast<R>(0.5);

    S_ += face_S_[i];
  }

  light_ids_ = std::shared_ptr<unsigned int[]>(new unsigned int[num_faces]);

  for (int i = 0;i < num_faces;i++) {
    light_ids_[i] = -1;
  }

}

void Mesh::set_material(const unsigned int material_id,const unsigned int prim_id) {
  if (prim_id < 0 || num_faces_ <= prim_id) {
    for (int i = 0;i < num_faces_;i++) {
      material_ids_[i] = material_id;
    }
  } else {
    if (prim_id < 0 || num_faces_ <= prim_id) {
      std::cerr << "error in func Mesh::set_light" << std::endl;
      return;
    }

    material_ids_[prim_id] = material_id;

  }
}

void Mesh::set_light(const unsigned int light_id,const unsigned int prim_id) {

  if (prim_id < 0 || num_faces_ <= prim_id) {
    for (int i = 0;i < num_faces_;i++) {
      light_ids_[i] = light_id;
    }
  } else {
    if (prim_id < 0 || num_faces_ <= prim_id) {
      std::cerr << "error in func Mesh::set_light" << std::endl;
      return;
    }

    light_ids_[prim_id] = light_id;

  }
}

Mesh::~Mesh() {
}

}
